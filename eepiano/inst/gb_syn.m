%% Copyright (C) 2008  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: gb_syn FILE
%%
%% Run an imy synthesis.

function gb_syn(file)
  dura = 0;
  note = 1:108;
  fs = 44100;
  f = 440 * 2.^((note-10)/12-3);
  T = 0.5 / 2.^(dura-2);
  t = 0:1/fs:T;
  fa = abs(1j+158.5./f);
  ta = 10.^(interp1([0 0.2 0.9 1]*T, -[1 1.8 2.1 3], t, 'pchip'));
  y = ifelse(mod(t.'*f,1)<0.5,-1,1) .* (ta.'*fa);
  p = cellfun(@audioplayer, num2cell(y,1), {fs}, 'UniformOutput',false);
  ocu.p = onCleanup(@() cellfun(@stop, p));
  n = 12*1+1:12*6+12;
  k='ZSXDCVGBHNJM<L>:?q2w3e4rt6y7ui9o0p-[zsxdcvgbhnjm,l.;/Q@W#E$RT^Y&UI(O)P_{';
  ind = zeros(1,127); ind(double(k)) = n;

  obj = gb_imy(file);
  for [v,k] = get(obj) disp(['  ' k ': ' v]); end
  [y,fs] = sound(obj); y = audioplayer(y,fs);
  ocu.y = onCleanup(@() stop(y));
  [img,pts] = image(obj); img = img.';

  chr = lookup([2 4 7 9 11], mod(note,12), 'b');
  im1 = zeros(size(chr));
  im2 = [zeros(8,1);ones(15,1)] * chr;
  im3 = zeros(pts, numel(chr));
  set(fig=clf, 'keypressfcn', {@kpf,p,ind}, 'windowbuttondownfcn', {@bdf,p,y});
  ocu.fig = onCleanup(@() isfigure(fig) && close(fig));
  axes('position',[0 0 1 1]); h = imshow([im1;im2;im3]); axis xy normal;
  n = 0;
  while (isfigure(fig))
    pb = cellfun(@isplaying, p);
    yb = isplaying(y);
    pd = cellfun(@(p) p.CurrentSample / p.TotalSamples, p(pb));
    yd = y.CurrentSample / y.TotalSamples;
    i1 = im1; i1(1:round(end*yd)) = 0.6;
    i2 = ifelse(im2,0.2,1); i2(:,pb) += (chr(pb)-0.5).*(1-pd);
    i3 = resize(img(1+round(end*yd):end,:), size(im3));
    set(h, 'cdata', [i1;i2;i3]); drawnow();
    pb |= cellfun(@isplaying, p);
    yb |= isplaying(y);
    if (~any([pb yb])) waitfor(fig, 'userdata'); end
    n = mod(n, numel(p)) + 1;
    if (~isplaying(p{n})) stop(p{n}); end
  end
end

function kpf(h,e, p,ind)
  n = ind(double(e.Character));
  if (n) play(p{n}); end
  set(h, 'userdata', []);
end

function bdf(h,e, p,y)
  switch (e)
  case 1
    n = round(get(gca,'currentpoint')(1));
    play(p{n});
  case 2
    yb = isplaying(y); yc = y.CurrentSample; yt = y.TotalSamples;
    yd = (round(get(gca,'currentpoint')(1)) - 1) / numel(p);
    if (yb || yc == yt)
      stop(y); play(y, yd*yt+1);
    else
      stop(y); play(y, yd*yt+[1 1]);
    end
  case 3
    yb = isplaying(y); yc = y.CurrentSample; yt = y.TotalSamples;
    if (yb)
      pause(y);
    elseif (yc == yt)
      stop(y);
    else
      stop(y); play(y, yc+1);
    end
  end
  set(h, 'userdata', []);
end
