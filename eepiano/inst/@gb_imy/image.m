%% Copyright (C) 2008  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: [IMG, PTS] = image (OBJ)
%% usage: [IMG, PTS] = image (OBJ, PTS)
%%
%% Get score image from an imy object with PTS points per note.

function [img, pts] = image(obj, pts=192)
  sr = obj.style;
  d = obj.duration;
  n = obj.note;
  v = obj.volume;

  d = ([sr;1-sr] * d)(:);
  n = ([1;1] * n)(:);
  v = ([1;0] * v)(:);
  tm = [d n v](d>0,:); [d,n,v] = num2cell(tm,1){:};
  d = cumsum(d);
  yd = 0.5/pts:1/pts:d(end);
  yn = interp1([0;d], [0;n], yd, 'next');
  yv = interp1([0;d], [0;v], yd, 'next');
  img = sparse([zeros(108,1) eye(108)])(:,yn+1) * diag(yv);
end
