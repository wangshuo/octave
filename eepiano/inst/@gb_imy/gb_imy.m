%% Copyright (C) 2008  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: OBJ = gb_imy (FILENAME)
%%
%% Create an imy object from the imy-file FILENAME.
%%
%% Reference: <http://merwin.bespin.org/t4a/specs/ems_imelody.txt>.

function obj = gb_imy(filename)
  if (isa(filename, 'gb_imy'))
    obj = filename;
    return
  end
  str = fileread(filename);

  str = regexprep(str, '\r\n( |\t)', '');
  imelody_object = regexp(str, strjoin({
      '(?i)BEGIN(?-i):IMELODY\r\n'
      '(?i)VERSION(?-i):(?<version>1\.2)\r\n'
      '(?i)FORMAT(?-i):(?<format>CLASS1\.0|CLASS2\.0)\r\n'
      '((?i)NAME(?-i):(?<name>[^\n]*)\r\n)?'
      '((?i)COMPOSER(?-i):(?<composer>[^\n]*)\r\n)?'
      '((?i)BEAT(?-i):(?<beat>900|[1-8]\d\d|[3-9]\d|2[5-9])\r\n)?'
      '((?i)STYLE(?-i):S?(?<style>[0-2])\r\n)?'
      '((?i)VOLUME(?-i):V?(?<volume>1[0-5]|\d)\r\n)?'
      '((?i)COPYRIGHT(?-i):(?<copyright>FREE|PROTECTED)\r\n)?'
      '(?i)MELODY(?-i):(?<melody>[^\n]*)\r\n'
      '(?i)END(?-i):IMELODY\r\n'
    }, ''), 'names');

  melody = imelody_object.melody;
  [melody, repeat] = regexp(melody, '\((?<melody>[^()]+)@(?<repeat_count>\d+)(?<volume>V[-+])?\)', 'split', 'names');
  repeat = cellfun(@imy_repeat, {repeat.melody}, {repeat.repeat_count}, {repeat.volume}, 'UniformOutput',false);
  melody = strjoin(melody, repeat);
  melody = regexp(melody, strjoin({
      'V(?<volume>1[0-5]|\d|[-+])'
      '(\*(?<octave_prefix>[0-8]))?(?<note>[cdefgab]|&[degab]|#[cdfga])(?<duration>[0-5])(?<duration_specifier>[.:;])?'
      '(?<rest>r)(?<duration>[0-5])(?<duration_specifier>[.:;])?'
      'led(?<led>off|on)'
      'vibe(?<vibe>off|on)'
      'back(?<backlight>off|on)'
    }, '|'), 'names');

  br = imy_beat(imelody_object.beat);
  sr = imy_style(imelody_object.style);
  imy_volume(imelody_object.volume, true);
  v = cellfun(@imy_volume, {melody.volume});
  n = cellfun(@imy_note, {melody.note}, {melody.octave_prefix});
  d = cellfun(@imy_duration, {melody.duration}, {melody.duration_specifier});
  tm = [d;n;v](:,d>0); [d,n,v] = num2cell(tm,2){:};
  t = 240/br * d;
  f = 880 * 2.^((n-10)/12-4) .* (n>0);
  a = mu2lin(ceil(255-127*v));

  obj.version = imelody_object.version;
  obj.format = imelody_object.format;
  obj.name = imelody_object.name;
  obj.composer = imelody_object.composer;
  obj.copyright = imelody_object.copyright;
  obj.beat = br;
  obj.style = sr;
  obj.duration = d;
  obj.note = n;
  obj.volume = v;
  obj.time = t;
  obj.frequency = f;
  obj.amplitude = a;
  obj = class(obj, 'gb_imy');
end

function melody = imy_repeat(melody, repeat_count, volume)
  %% NOTE: not (0 is repeat forever)
  melody = repmat([melody volume], 1, str2double(repeat_count)+1);
end

function br = imy_beat(beat)
  br = 120;
  if (beat) br = str2double(beat); end
end

function sr = imy_style(style)
  s = 0;
  if (style) s = str2double(style); end
  sr = [20/21 1 1/2](s+1);
end

function v = imy_volume(volume, init=false)
  persistent s; if (init) s = 7; end
  if (volume) switch (volume)
  case '+' s = min(s+1, 15);
  case '-' s = max(s-1, 0);
  otherwise s = str2double(volume);
  end end
  v = (s+1)/16 .* (s>0);
end

function n = imy_note(note, octave_prefix)
  s = struct( 'c',  1, '#c',  2,
    '&d',  2, 'd',  3, '#d',  4,
    '&e',  4, 'e',  5,
              'f',  6, '#f',  7,
    '&g',  7, 'g',  8, '#g',  9,
    '&a',  9, 'a', 10, '#a', 11,
    '&b', 11, 'b', 12);
  n = 0;
  if (note) n = s.(note) + 12*4; end
  if (octave_prefix) n += 12*(str2double(octave_prefix)-4); end
end

function d = imy_duration(duration, duration_specifier)
  s = struct('.', 1.5, ':', 1.75, ';', 2/3);
  d = 0;
  if (duration) d = 2^(-str2double(duration)); end
  if (duration_specifier) d *= s.(duration_specifier); end
end
