%% Copyright (C) 2008  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: [Y, FS] = sound (OBJ)
%% usage: [Y, FS] = sound (OBJ, FS)
%%
%% Get audio data from an imy object at sample rate FS.

function [y, fs] = sound(obj, fs=44100)
  sr = obj.style;
  t = obj.time;
  f = obj.frequency;
  a = obj.amplitude;

  t = ([sr;1-sr] * t)(:);
  f = ([1;1] * f)(:);
  a = ([1;0] * a)(:);
  tm = [t f a](t>0,:); [t,f,a] = num2cell(tm,1){:};
  r = cumsum(t .* f);
  t = cumsum(t);
  yt = 0.5/fs:1/fs:t(end);
  yr = interp1([0;t], [0;r], yt);
  ya = interp1([0;t], [0;a], yt, 'next');
  y = ifelse(mod(yr,1)<0.5,-1,1) .* ya;
end
