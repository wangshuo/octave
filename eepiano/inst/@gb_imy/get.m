%% Copyright (C) 2008  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: INFO = get (OBJ)
%%
%% Get information from an imy object.

function info = get(obj)
  info.version = obj.version;
  info.format = obj.format;
  info.name = obj.name;
  info.composer = obj.composer;
  info.copyright = obj.copyright;
end
