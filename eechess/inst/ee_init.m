%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: ee_init
%%
%% Run an example machine.
%%
%% Press <E> <D> <S> <F> to move.  Strictly follow the law of inertia.
%% Press <J> <K> to use/select items.  The cross is the most powerful.

function ee_init(hard=0)
  p = fullfile(fileparts(mfilename('fullpath')), {'class','image','sound'});
  addpath(p{:});
  ocu = onCleanup(@() rmpath(p{:}));
  sig.ee_att = zeros(0,2);
  sig.ee_def = zeros(0,2);
  sig.ee_hero = zeros(0,2);
  sig.ee_hpup = zeros(0,2);
  sig.ee_htup = zeros(0,2);
  sig.ee_sclrd = false;
  sig.ee_sdead = false;
  sig.ee_skill = false;
  sig.ee_tstop = false;
  sig.ee_using = false;
  obj{1} = ifelse(hard < 0, ee_test(), ee_demo1(hard));
  fc_init(obj, sig);
end
