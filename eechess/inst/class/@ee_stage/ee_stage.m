%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function obj = ee_stage(stage, hard)
  fg = 64*[1 1 0; 1 1 0; 1 1 0; 1 1 0; 0 1 1; 1 0 1](stage,:);
  bg = 64*[1 1 1; 1 0 0; 0 0 1; 0 0 0; 1 1 1; 0 0 0](stage,:);
  fg = repmat(shiftdim(uint8(fg),-1), 32);
  bg = repmat(shiftdim(uint8(bg),-1), 32);
  obj.show{1} = repmat([fg bg;bg fg], 4);
  obj.show{2} = [0 0];

  obj.play.snorm{1} = {'vk.mid' 'stalker.mid' 'wickedchild.mid' 'walkingontheedge.mid' 'heartoffire.mid' 'outoftime.mid'}{stage};
  obj.play.sdead{1} = 'death.mid';
  obj.play.sclrd{1} = ifelse(stage < 6, 'stageclear.mid', 'allclear.mid');
  obj.play.snorm{2} = [2 1];
  obj.play.sdead{2} = [1 1];
  obj.play.sclrd{2} = [1 1];

  obj.enemy = sparse(1,6);
  obj.enemy(1,:) = -1;
  obj.enemy(32*64,:) = 11:16;
  obj.enemy(32*(4:4:20),1) = 1; obj.enemy(32*(32:4:48),1) = 4;
  obj.enemy(32*(4:4:20),2) = 3; obj.enemy(32*(32:4:48),2) = 2;
  obj.enemy(32*(2:2:20),3) = 4; obj.enemy(32*(30:2:48),3) = 2;
  obj.enemy(32*(2:2:20),4) = 1; obj.enemy(32*(30:2:48),4) = 3;
  obj.enemy(32*(4:4:20),5) = 4; obj.enemy(32*(32:4:48),5) = 2;
  obj.enemy(32*(2:4:20),5) = 2; obj.enemy(32*(30:4:48),5) = 3;
  obj.enemy(32*(4:4:20),6) = 4; obj.enemy(32*(32:4:48),6) = 1;
  obj.enemy(32*(2:4:20),6) = 1; obj.enemy(32*(30:4:48),6) = 3;
  obj.enemy = obj.enemy(end:-1:1, stage);

  obj.stage = stage;
  obj.hard = hard;
  if (hard)
    obj.hpmax = 32*2;
    obj.htmax = 15;
  else
    obj.hpmax = 32*4;
    obj.htmax = 30;
  end
  obj.st = 'snorm';
  obj.ct = obj.ct0 = numel(obj.enemy);
  obj = class(obj, 'ee_stage');
end
