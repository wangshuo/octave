%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function objs = set(obj, sig)
  switch (obj.st)
  case 'sdead'
    if (obj.ct > 0)
      obj.ct--;
      objs = {obj};
    else
      objs = {ee_demo3(obj.stage, obj.hard)};
    end
  case 'sclrd'
    if (obj.ct > 0)
      obj.ct--;
      objs = {obj};
    elseif (obj.stage < 6)
      objs = {ee_stage(obj.stage+1, obj.hard)};
    else
      objs = {ee_demo2(obj.hard+1)};
    end
  otherwise
    if (sig.ee_sdead)
      obj.st = 'sdead';
      obj.ct = obj.ct0 = 32*5;
      objs = {obj};
    elseif (sig.ee_sclrd)
      obj.st = 'sclrd';
      obj.ct = obj.ct0 = 32*7;
      objs = {obj};
    elseif (obj.ct > 0)
      switch (obj.enemy(obj.ct))
      case 1 enemy = {ee_enemy1()};
      case 2 enemy = {ee_enemy2()};
      case 3 enemy = {ee_enemy3()};
      case 4 enemy = {ee_enemy4()};
      case 11 enemy = {ee_boss1()};
      case 12 enemy = {ee_boss2()};
      case 13 enemy = {ee_boss3()};
      case 14 enemy = {ee_boss4()};
      case 15 enemy = {ee_boss5()};
      case 16 enemy = {ee_boss6()};
      case -1 enemy = {ee_hero(obj.hpmax,obj.htmax)};
      otherwise enemy = {};
      end
      obj.ct--;
      objs = {obj, enemy{:}};
    else
      objs = {obj};
    end
  end
end
