%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function objs = set(obj, sig)
  if (sig.ee_skill)
    objs = {};
    return
  end
  obj.init = false;
  enemy = {};
  if (obj.hp > 0)
    if (any(all(abs(obj.xy - sig.ee_att) <= 32/2, 2)))
      obj.hp--;
      obj.hurt = true;
    else
      obj.hurt = false;
    end
    if (~sig.ee_tstop)
      obj.ct--;
      if (obj.ct <= -32*2)
        obj.ct = 32*randi([0,1]);
        if (numel(sig.ee_hero))
          vxy = sig.ee_hero - obj.xy;
          vxy = round(1.3*sign(complex(vxy(1), vxy(2))));
          vxy = [real(vxy) imag(vxy)];
          obj.vxy = vxy;
        end
      end
      obj.vxy(obj.vxy < 0 & obj.xy <= 0) = 1;
      obj.vxy(obj.vxy > 0 & obj.xy >= 256-32) = -1;
      obj.xy += obj.vxy;
      assert(obj.xy >= 0 && obj.xy <= 256-32);
    end
    objs = {obj, enemy{:}};
  else
    objs = {ee_hpup(obj.xy)};
  end
end
