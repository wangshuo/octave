%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function obj = ee_boss3(hpmax=32*16)
  obj.show.hp = zeros([6 32*6 3], 'uint8');
  obj.show.hp(:,:,[1 2]) = 128;
  obj.xy = [32*4 0];
  obj.vxy = [0 1];
  obj.hp = obj.hpmax = hpmax;
  obj.hurt = false;
  obj.init = true;
  obj.ct = 0;
  obj = class(obj, 'ee_boss3');
end
