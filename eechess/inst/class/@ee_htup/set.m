%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function objs = set(obj, sig)
  if (sig.ee_skill)
    objs = {};
    return
  end
  if (obj.ct > 0)
    if (abs(obj.xy - sig.ee_hero) <= 32/2)
      objs = {};
      return
    end
    if (~sig.ee_tstop)
      obj.ct--;
    end
    objs = {obj};
  else
    objs = {};
  end
end
