%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function sig = get(obj, sig)
  show{1} = ifelse(obj.hurt, 'candle.png', 'death.png');
  show{2} = obj.xy;
  sig.fc_show(end+1,:) = show;
  sig.ee_def(end+1,:) = obj.xy;
  if (obj.init)
    sig.fc_play(end+1,:) = {'poisonmind.mid', [2 1]};
  end
  show{1} = obj.show.hp(:,1:round(end*obj.hp/obj.hpmax),:);
  show{2} = [32 32*6+5];
  sig.fc_show(end+1,:) = show;
end
