%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function objs = set(obj, sig)
  if (sig.ee_skill)
    objs = {};
    return
  end
  if (~sig.ee_using)
    obj.item = ~obj.item;
  else
    obj.item = false;
  end
  power = {};
  if (obj.hp > 0 && ~obj.sclrd)
    if (any(all(abs(obj.xy - sig.ee_def) <= 32/2, 2)))
      obj.hp--;
      obj.hurt = true;
    else
      obj.hurt = false;
    end
    if (any(all(abs(obj.xy - sig.ee_hpup) <= 32/2, 2)))
      obj.hp = obj.hpmax;
      obj.ht = obj.htmax;
      obj.sclrd = true;
    end
    if (numel(sig.ee_htup))
      obj.ht += sum(all(abs(obj.xy - sig.ee_htup) <= 32/2, 2));
      if (obj.ht > obj.htmax) obj.ht = obj.htmax; end
    end

    ctrl = sig.fc_ctrl;
    if (ctrl(1) == 1) obj.vxy = sign(obj.vxy - [0 1]); end
    if (ctrl(2) == 1) obj.vxy = sign(obj.vxy + [0 1]); end
    if (ctrl(3) == 1) obj.vxy = sign(obj.vxy - [1 0]); end
    if (ctrl(4) == 1) obj.vxy = sign(obj.vxy + [1 0]); end
    if (ctrl(6) == 1)
      if (~sig.ee_using)
        obj.sel++; if (obj.sel > 5) obj.sel = 1; end
      end
    end
    if (ctrl(5) == 1)
      use = [3 3 3 3 5](obj.sel);
      if (~sig.ee_using && obj.ht >= use)
        obj.ht -= use;
        switch (obj.sel)
        case 1 power = {ee_power1(obj.xy, [ 0 -1])
                        ee_power1(obj.xy, [ 1 -1])
                        ee_power1(obj.xy, [ 1  0])
                        ee_power1(obj.xy, [ 1  1])
                        ee_power1(obj.xy, [ 0  1])
                        ee_power1(obj.xy, [-1  1])
                        ee_power1(obj.xy, [-1  0])
                        ee_power1(obj.xy, [-1 -1])};
        case 2 power = {ee_power2(obj.xy, 60*0)
                        ee_power2(obj.xy, 60*1)
                        ee_power2(obj.xy, 60*2)
                        ee_power2(obj.xy, 60*3)
                        ee_power2(obj.xy, 60*4)
                        ee_power2(obj.xy, 60*5)};
        case 3 power = {ee_power3()};
        case 4 power = {ee_power4([obj.xy(1) 32*4], 90*0)
                        ee_power4([obj.xy(1) 32*5], 90*1)
                        ee_power4([obj.xy(1) 32*6], 90*2)
                        ee_power4([obj.xy(1) 32*7], 90*3)};
        case 5 power = {ee_power5()};
        end
      end
    end
    obj.vxy(obj.vxy < 0 & obj.xy <= 0) = 0;
    obj.vxy(obj.vxy > 0 & obj.xy >= 256-32) = 0;
    obj.xy += obj.vxy;
    assert(obj.xy >= 0 && obj.xy <= 256-32);
  end
  objs = {obj, power{:}};
end
