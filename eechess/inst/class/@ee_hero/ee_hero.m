%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function obj = ee_hero(hpmax,htmax)
  obj.show.hp = zeros([6 32*4 3], 'uint8');
  obj.show.hp(:,:,[2 3]) = 128;
  obj.show.ht = zeros([6 32*4 3], 'uint8');
  obj.show.ht(:,:,[1 3]) = 128;
  obj.xy = 32*[3 4];
  obj.vxy = [0 0];
  obj.hp = obj.hpmax = hpmax;
  obj.ht = obj.htmax = htmax;
  obj.sel = 1;
  obj.sclrd = false;
  obj.hurt = false;
  obj.item = false;
  obj = class(obj, 'ee_hero');
end
