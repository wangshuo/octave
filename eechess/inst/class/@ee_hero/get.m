%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function sig = get(obj, sig)
  if (obj.hp > 0)
    show{1} = ifelse(obj.hurt, 'candle.png', 'simon.png');
    show{2} = obj.xy;
    sig.fc_show(end+1,:) = show;
    sig.ee_hero = obj.xy;
    if (~obj.sclrd)
      show{1} = 'whip.png';
      for dy = 32*(-2:-1)
        show{2} = obj.xy + [0 dy];
        sig.fc_show(end+1,:) = show;
        sig.ee_att(end+1,:) = show{2};
      end
    else
      sig.ee_sclrd = true;
    end
  else
    sig.ee_sdead = true;
  end

  show{1} = 'status.png';
  show{2} = [0 0];
  sig.fc_show(end+1,:) = show;
  show{1} = obj.show.hp(:,1:round(end*obj.hp/obj.hpmax),:);
  show{2} = [32 5];
  sig.fc_show(end+1,:) = show;
  show{1} = obj.show.ht(:,1:round(end*obj.ht/obj.htmax),:);
  show{2} = [32 21];
  sig.fc_show(end+1,:) = show;

  show = {'dagger.png', 32*[0 7]
          'axe.png'   , 32*[1 7]
          'water.png' , 32*[2 7]
          'cross.png' , 32*[3 7]
          'watch.png' , 32*[4 7]};
  if (obj.item)
    show(obj.sel,:) = [];
  end
  sig.fc_show(end+(1:rows(show)),:) = show;
end
