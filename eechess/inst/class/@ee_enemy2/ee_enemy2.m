%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function obj = ee_enemy2(left=(rand<0.5), y=32*randi([1,6]))
  if (left)
    obj.x = -32;
    obj.vx = 1;
  else
    obj.x = 256;
    obj.vx = -1;
  end
  obj.y = y;
  obj.p = 90;
  obj.xy = [obj.x round(obj.y+32*cosd(obj.p))];
  obj.hp = 32/2;
  obj.hurt = false;
  obj = class(obj, 'ee_enemy2');
end
