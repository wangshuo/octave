%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function sig = get(obj, sig)
  show{1} = 'intro.png';
  show{2} = [0 -max(0,256+obj.ct-obj.ct0)];
  sig.fc_show(end+1,:) = show;
  if (obj.ct >= obj.ct0)
    sig.fc_play(end+1,:) = {'prologue.mid', [1 1]};
  end
end
