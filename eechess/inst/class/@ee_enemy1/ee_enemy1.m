%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function obj = ee_enemy1(x=32*randi([0,6]), p=90*randi(4))
  obj.x = x;
  obj.p = p;
  obj.y = -32;
  obj.xy = [round(obj.x+32*(1+cosd(obj.p))/2) obj.y];
  obj.hp = 32/2;
  obj.hurt = false;
  obj = class(obj, 'ee_enemy1');
end
