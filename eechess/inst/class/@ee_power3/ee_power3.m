%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function obj = ee_power3()
  im = kron(flip(eye(32)), [1;1]);
  map = linspace([96 192 192], [0 0 192], 32).';
  obj.show.storm = uint8(im .* shiftdim(map,-1));
  obj.ct = 32;
  obj = class(obj, 'ee_power3');
end
