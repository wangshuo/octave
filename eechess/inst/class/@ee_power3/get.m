%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function sig = get(obj, sig)
  show{1} = obj.show.storm;
  for i = 1:3
    show{2} = randi(256, 1,2) - size(show{1}(:,:,1).')/2;
    sig.fc_show(end+1,:) = show;
  end
  [x,y] = ndgrid(32*(0:7));
  sig.ee_att(end+(1:64),:) = [x(:) y(:)];
  sig.ee_using = true;
end
