%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function obj = ee_test()
  obj.im = num2cell(ones(10,'uint8').*shiftdim(eye(3)*255,-2), 1:3);
  obj.xy = num2cell([0 256-30]+10*[1 0;1 2;0 1;2 1;5 2;7 2;5 0;7 0], 2);
  fs = 8000; y = resize(randn(fs*0.3,1)*0.01, fs*0.5,1);
  obj.fn = {{y,fs} 'death.mid'};
  obj.ctrl = zeros(1,8);
  obj = class(obj, 'ee_test');
end
