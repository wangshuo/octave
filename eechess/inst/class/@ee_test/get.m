%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function sig = get(obj, sig)
  for i = 1:8 if (obj.ctrl(i))
    sig.fc_show(end+1,:) = {obj.im{obj.ctrl(i)}, obj.xy{i}};
  end end
  for i = [5 5 6 6;1 2 1 2;2 0 1 0;1 1 2 2] if (obj.ctrl(i(1)) == i(2))
    sig.fc_play(end+1,:) = {obj.fn{i(4)}, i(3:4)};
  end end
end
