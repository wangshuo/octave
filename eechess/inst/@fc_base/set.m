%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function objs = set(obj, sig)
  if (~isfigure(obj.fig))
    objs = {};
    return
  end
  obj.key = [obj.key(2,:); get(obj.fig, 'userdata')];

  for p = sig.fc_play.'
    fn = p{1}; op = p{2}(1); po = p{2}(2);
    if (op == 0)
      obj.pid{po} = [];
    elseif (ischar(fn))
      op = ['exec timidity --quiet ' {'' '--loop'}{op} ' '];
      fn = ['"' regexprep(file_in_loadpath(fn), '[$`"\\]', '\\$0') '"'];
      pid = system([op fn], [], 'async');
      obj.pid{po} = onCleanup(@() pid>0 && [kill(pid,SIG.TERM) waitpid(pid)]);
    else
      fn{1} = repmat(fn{1}, op,1);
      pid = audioplayer(fn{:});
      play(pid);
      obj.pid{po} = onCleanup(@() stop(pid));
    end
  end

  dst = obj.dst;
  for s = sig.fc_show.'
    src = s{1}; x = s{2}(1); y = s{2}(2);
    if (ischar(src))
      if (~isfield(obj.src, src))
        obj.src.(src) = imread(file_in_loadpath(src));
      end
      src = obj.src.(src);
    end
    xd = max(x+1,1) : min(x+columns(src),columns(dst));
    yd = max(y+1,1) : min(y+rows(src),rows(dst));
    xs = xd - x;
    ys = yd - y;
    dst(yd,xd,:) += src(ys,xs,:);
  end
  set(obj.hdl, 'cdata', dst); drawnow();

  obj.frame++;
  dt = obj.frame/32 - toc(obj.id);
  pause(dt);
  if (dt < -8/32)
    obj.frame = 0;
    obj.id = tic();
    warning('frame reset');
  end
  objs = {obj};
end
