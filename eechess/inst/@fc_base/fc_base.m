%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

function obj = fc_base()
  obj.fig = fig = clf;
  set(fig, 'keypressfcn',@kpf, 'keyreleasefcn',@krf, 'userdata',false(1,127));
  obj.key = false(2,127);
  obj.ocu = onCleanup(@() isfigure(fig) && close(fig));

  obj.pid = {};
  obj.src = struct();
  obj.dst = zeros([256 256 3], 'uint8');
  obj.hdl = imshow(obj.dst);

  obj.frame = 0;
  obj.id = tic();
  obj = class(obj, 'fc_base');
end

function kpf(h, s)
  k = get(h, 'userdata');
  k(double(s.Character)) = 1;
  set(h, 'userdata', k);
end
function krf(h, s)
  k = get(h, 'userdata');
  k(double(s.Character)) = 0;
  set(h, 'userdata', k);
end
