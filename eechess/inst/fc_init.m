%% Copyright (C) 2006  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: fc_init (OBJ, SIG)
%%
%% Run an FC machine until all objects die.
%% OBJ is a cell array of initial objects.
%% SIG is a structure of background signals (optional).
%%
%% usage: SIG = get (OBJ, SIG)
%%
%% Object method to send signals.
%% SIG is a structure of signals already from some other objects.
%% Output a structure of signals also from OBJ.
%%
%% usage: OBJ = set (OBJ, SIG)
%%
%% Object method to receive signals.
%% SIG is a structure of signals from all objects.
%% Output a cell array of updated OBJ if not died and new born objects.
%%
%% usage: SIG.fc_ctrl
%%
%% Signal from controller.
%% An 8-element vector of <E> <D> <S> <F> <J> <K> <G> <H> status,
%% which may be 0, 1, 2, 3 for released, pressing, releasing, pressed.
%%
%% usage: SIG.fc_play
%%
%% Signals to sound player.
%% A 2-column cell array of mid-files in the first column
%% and repeat counts and port numbers in the second column.
%%
%% usage: SIG.fc_show
%%
%% Signals to image displayer.
%% A 2-column cell array of png-files or images in the first column
%% and left top locations of the images in the second column.

function fc_init(obj={}, sig=struct())
  sig.fc_ctrl = zeros(1,8);
  sig.fc_play = cell(0,2);
  sig.fc_show = cell(0,2);
  obj = [{fc_base()} obj];
  while (numel(obj) > 1 && isa(obj{1}, 'fc_base'))
    sigs = sig;
    for i = 1:numel(obj)
      sigs = get(obj{i}, sigs);
    end
    objs = {};
    for i = 1:numel(obj)
      objs = [objs set(obj{i}, sigs)];
    end
    obj = objs;
  end
end
