%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: R = as_setg (X, Y, Z, G)
%% usage: [RX, RY, RZ] = as_setg (...)
%%
%% Generate a full 3-D grid locations.
%%
%% X, Y, Z are coordinates, sorted real vector.
%%
%% G specifies grid type,
%%   0 for node grid,
%%   1 for branch grid,
%%   2 for loop grid.
%%
%% R is node grid locations, real matrix.
%% RX, RY, RZ are branch/loop grid locations, real matrix.
%%
%% See also: as_matf.

function [rx,ry,rz] = as_setg(x,y,z, g)

  xn = x(1:end-1); xp = x(2:end);
  yn = y(1:end-1); yp = y(2:end);
  zn = z(1:end-1); zp = z(2:end);
  switch (g)
  case 0
    rx = as_xyz(x,y,z);
  case 1
    rx = [as_xyz(xn,y,z), as_xyz(xp,y,z)];
    ry = [as_xyz(x,yn,z), as_xyz(x,yp,z)];
    rz = [as_xyz(x,y,zn), as_xyz(x,y,zp)];
  case 2
    rx = [as_xyz(x,yn,zn), as_xyz(x,yp,zn), as_xyz(x,yp,zp), as_xyz(x,yn,zp)];
    ry = [as_xyz(xn,y,zn), as_xyz(xn,y,zp), as_xyz(xp,y,zp), as_xyz(xp,y,zn)];
    rz = [as_xyz(xn,yn,z), as_xyz(xp,yn,z), as_xyz(xp,yp,z), as_xyz(xn,yp,z)];
  end

end

function r = as_xyz(x,y,z)
  [x,y,z] = ndgrid(x,y,z);
  r = [x(:) y(:) z(:)];
end
