%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: H = as_plotr (R)
%% usage: H = as_plotr (R, STYLE)
%%
%% Plot locations.
%%
%% R is locations, real matrix.
%%
%% STYLE is the style to use for the plot.
%%
%% H is the optional graphics handle to the created plot.
%%
%% See also: as_matf.

function h = as_plotr(r, varargin)

  switch (columns(r))
  case 3
    h = as_plotd(r, 1, :, varargin{:});
  case 6
    [rn,rp] = as_branch(r);
    h = as_plotd((rp+rn)/2, rp-rn, :, varargin{:});
  case 12
    [r1,r2,r3,r4] = as_loop(r);
    h = as_plotr([r1;r2;r3;r4], varargin{:});
  end

end
