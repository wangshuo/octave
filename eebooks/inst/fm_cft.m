%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: [XF, F] = fm_cft (XT, T)
%% usage: [XF, F] = fm_cft (XT, T, DIM)
%%
%% Compute the continuous-time Fourier transform.
%%
%% See also: fm_icft.

function [x, f] = fm_cft(x, t, varargin)
  [f, n, ts, tp] = fm_tfparams(t);
  x = fm_sfft(x, n, varargin{:}) * ts;
end

%!demo
%! close all;
%! tol = eps^0.5;
%! n = 80; tp = 2; ts = tp/n; fp = 1/tp; fs = 1/ts;
%! T = tp/10;
%! t = -tp/2:ts:tp/2-ts/2;
%! x = exp(-pi*(t/T).^2);
%! [X, f] = fm_cft(x, t);
%! assert(f/fp, t/ts, tol);
%! assert(X/T, exp(-pi*(f*T).^2), tol); X = real(X);
%! subplot(2,1,1); plot([t-tp t t+tp],[x*0 x x*0]);
%! xlabel s; ylabel V; ylim([0 1]);
%! subplot(2,1,2); plot([f-fs f f+fs],[X*0 X X*0]);
%! xlabel Hz; ylabel V/Hz; ylim([0 T]);
