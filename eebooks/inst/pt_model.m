%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: MODEL = pt_model (TYPE, ...)
%%
%% Define model of certain type, see below for details.
%% MODEL is the horizontal concatenation of NP-by-(NP*2) matrix A and
%% NP-element column vector B constraining as (A * [V;I] = B) where
%% V and I are port voltage and current, NP-element column vectors.
%%
%% usage: MODEL = pt_model ('1i', I, Y)
%%
%% Define model of current source.
%% I is current source from ground to port, NL-element vector.
%% Y is optional admittance, NL-by-NL matrix.
%% The direction of all NL ports are output.
%%
%% usage: MODEL = pt_model ('1v', V, Z)
%%
%% Define model of voltage source.
%% V is voltage source from port to ground, NL-element vector.
%% Z is optional impedance, NL-by-NL matrix.
%% The direction of all NL ports are output.
%%
%% usage: MODEL = pt_model ('1y', Y, I)
%%
%% Define model of admittance.
%% Y is admittance, NL-by-NL matrix.
%% I is optional current source from port to ground, NL-element vector.
%% The direction of all NL ports are input.
%%
%% usage: MODEL = pt_model ('1z', Z, V)
%%
%% Define model of impedance.
%% Z is impedance, NL-by-NL matrix.
%% V is optional voltage source from port to ground, NL-element vector.
%% The direction of all NL ports are input.
%%
%% usage: MODEL = pt_model ('2a', V, I)
%%
%% Define model of sources.
%% V is voltage source from input to output, NL-element vector.
%% I is current source from port to ground, NL-element vector.
%% The direction of ports 1:NL are input, and NL+1:NL*2 are output.
%%
%% usage: MODEL = pt_model ('2p', Z, Y)
%%
%% Define model of transmission line.
%% Z is impedance of total length, NL-by-NL matrix.
%% Y is admittance of total length, NL-by-NL matrix.
%% The direction of ports 1:NL are input, and NL+1:NL*2 are output.
%%
%% usage: MODEL = pt_model ('2y', Y, I)
%%
%% Define model of admittance.
%% Y is admittance, NL-by-NL matrix.
%% I is optional current source from port to ground, NL-element vector.
%% The direction of ports 1:NL are input, and NL+1:NL*2 are output.
%%
%% usage: MODEL = pt_model ('2z', Z, V)
%%
%% Define model of impedance.
%% Z is impedance, NL-by-NL matrix.
%% V is optional voltage source from input to output, NL-element vector.
%% The direction of ports 1:NL are input, and NL+1:NL*2 are output.
%%
%% usage: MODEL = pt_model ('nb', PORTDIR, NL)
%%
%% Define model of branches.
%% Every NL (default is 1) ports form a bus.
%% The direction of buses are specified by vector PORTDIR with 1 or -1.
%%
%% See also: pt_subckt, pt_ac.

function model = pt_model(tt, varargin)
  tt = str2func(['pt_model_' tt]);
  switch (tt)
  case localfunctions
    model = tt(varargin{:});
  end
end

function model = pt_model_1i(I=zeros(1,1), Y=zeros(numel(I)))
  model = pt_model_1y(-Y, I);
end

function model = pt_model_1v(V=zeros(1,1), Z=zeros(numel(V)))
  model = pt_model_1z(-Z, V);
end

function model = pt_model_1y(Y=zeros(1), I=zeros(length(Y),1))
  assert(issquare(Y));
  assert(isvector(I)); I = I(:);
  assert(length(Y), numel(I));
  nl = length(Y);
  model = [-Y eye(nl) I];
end

function model = pt_model_1z(Z=zeros(1), V=zeros(length(Z),1))
  assert(issquare(Z));
  assert(isvector(V)); V = V(:);
  assert(length(Z), numel(V));
  nl = length(Z);
  model = [eye(nl) -Z V];
end

function model = pt_model_2a(V=zeros(1,1), I=zeros(1,1))
  assert(isvector(V)); V = V(:);
  assert(isvector(I)); I = I(:);
  assert(numel(V), numel(I));
  nl = numel(V);
  T = eye(nl*2);
  B = [V; I];
  deinter = kron(eye(4)(:,[1 3 2 4]), eye(nl));
  A = [eye(nl*2) -T] * deinter;
  model = [A B];
end

function model = pt_model_2p(Z=zeros(1), Y=zeros(1))
  assert(issquare(Z));
  assert(issquare(Y));
  assert(length(Z), length(Y));
  nl = length(Z);
  T = expm([zeros(nl) Z; Y zeros(nl)]);
  B = zeros(nl*2,1);
  deinter = kron(eye(4)(:,[1 3 2 4]), eye(nl));
  A = [eye(nl*2) -T] * deinter;
  model = [A B];
end

function model = pt_model_2y(Y=zeros(1), I=zeros(length(Y),1))
  assert(issquare(Y));
  assert(isvector(I)); I = I(:);
  assert(length(Y), numel(I));
  nl = length(Y);
  T = [eye(nl) zeros(nl); Y eye(nl)];
  B = [zeros(nl,1); I];
  deinter = kron(eye(4)(:,[1 3 2 4]), eye(nl));
  A = [eye(nl*2) -T] * deinter;
  model = [A B];
end

function model = pt_model_2z(Z=zeros(1), V=zeros(length(Z),1))
  assert(issquare(Z));
  assert(isvector(V)); V = V(:);
  assert(length(Z), numel(V));
  nl = length(Z);
  T = [eye(nl) Z; zeros(nl) eye(nl)];
  B = [V; zeros(nl,1)];
  deinter = kron(eye(4)(:,[1 3 2 4]), eye(nl));
  A = [eye(nl*2) -T] * deinter;
  model = [A B];
end

function model = pt_model_nb(portdir=[1], nl=1)
  assert(isvector(portdir)); portdir = portdir(:).';
  assert(isscalar(nl));
  nb = numel(portdir);
  A = kron(blkdiag(diff(eye(nb),[],1), portdir), eye(nl));
  B = zeros(nl*nb,1);
  model = [A B];
end

%!shared tol
%! tol = eps^0.5;
%!function check(tol, m1, m2)
%! ms = pt_model('1v', 11:13);
%! ml = pt_model('1z', magic(3));
%! [v1,i1] = pt_ac(1:3,ms, 1:6,m1, 4:6,ml);
%! [v2,i2] = pt_ac(1:3,ms, 1:6,m2, 4:6,ml);
%! assert(v1, v2, tol); assert(i1, i2, tol);
%!endfunction

%!test
%! check(tol, pt_subckt([1:3 7:9],
%!       1:6, pt_model('2a', 11:13, 31:33),
%!       4:9, pt_model('2a', 21:23, 41:43),
%!   'ends'), pt_model('2a', 32:2:36, 72:2:76));
%!test
%! check(tol, pt_subckt([1:3 7:9],
%!       1:6, pt_model('2p', magic(3)*0.2, diag(11:13)*0.2),
%!       4:9, pt_model('2p', magic(3)*0.3, diag(11:13)*0.3),
%!   'ends'), pt_model('2p', magic(3)*0.5, diag(11:13)*0.5));
%!test
%! check(tol, pt_subckt([1:3 7:9],
%!       1:6, pt_model('2y', magic(3)*2, 11:13),
%!       4:9, pt_model('2y', magic(3)*3, 21:23),
%!   'ends'), pt_model('2y', magic(3)*5, 32:2:36));
%!test
%! check(tol, pt_subckt([1:3 7:9],
%!       1:6, pt_model('2z', magic(3)*2, 11:13),
%!       4:9, pt_model('2z', magic(3)*3, 21:23),
%!   'ends'), pt_model('2z', magic(3)*5, 32:2:36));

%!test
%! assert(pt_model('1i'), pt_model('1y'));
%! assert(pt_model('1i', 11, 21), pt_model('1y', -21, 11));
%! assert(pt_model('1i', zeros(3,1)), pt_model('1y', zeros(3)));
%! assert(pt_model('1i', 11:13, magic(3)), pt_model('1y', -magic(3), 11:13));

%!test
%! assert(pt_model('1v'), pt_model('1z'));
%! assert(pt_model('1v', 11, 21), pt_model('1z', -21, 11));
%! assert(pt_model('1v', zeros(3,1)), pt_model('1z', zeros(3)));
%! assert(pt_model('1v', 11:13, magic(3)), pt_model('1z', -magic(3), 11:13));

%!test
%! assert(pt_model('1y'), [0 1 0]);
%! assert(pt_model('1y', 11, 21), [-11 1 21]);
%! assert(pt_model('1y', zeros(3)), [zeros(3) eye(3) zeros(3,1)]);
%! assert(pt_model('1y', magic(3), 11:13), [-magic(3) eye(3) (11:13).']);

%!test
%! assert(pt_model('1z'), [1 0 0]);
%! assert(pt_model('1z', 11, 21), [1 -11 21]);
%! assert(pt_model('1z', zeros(3)), [eye(3) zeros(3) zeros(3,1)]);
%! assert(pt_model('1z', magic(3), 11:13), [eye(3) -magic(3) (11:13).']);

%!test
%! assert(pt_model('2a'), [
%!    1 -1  0  0  0
%!    0  0  1 -1  0
%!   ]);
%! assert(pt_model('2a', 21, 11), [
%!    1 -1  0  0  21
%!    0  0  1 -1  11
%!   ]);
%! assert(pt_model('2a', zeros(3,1), zeros(3,1)), [
%!   eye(3) -eye(3) zeros(3) zeros(3) zeros(3,1)
%!   zeros(3) zeros(3) eye(3) -eye(3) zeros(3,1)
%!   ]);
%! assert(pt_model('2a', 21:23, 11:13), [
%!   eye(3) -eye(3) zeros(3) zeros(3) (21:23).'
%!   zeros(3) zeros(3) eye(3) -eye(3) (11:13).'
%!   ]);

%!test
%! z = 3; y = 2; t = expm([0 z;y 0]);
%! assert(pt_model('2p'), [
%!    1 -1  0  0  0
%!    0  0  1 -1  0
%!   ]);
%! assert(pt_model('2p', z, y), [
%!    1 -t(1)  0 -t(3)  0
%!    0 -t(2)  1 -t(4)  0
%!   ], tol);
%! assert(pt_model('2p', zeros(3), zeros(3)), [
%!   eye(3) -eye(3) zeros(3) zeros(3) zeros(3,1)
%!   zeros(3) zeros(3) eye(3) -eye(3) zeros(3,1)
%!   ]);
%! assert(pt_model('2p', z*eye(3), y*eye(3)), [
%!   eye(3) -t(1)*eye(3) zeros(3) -t(3)*eye(3) zeros(3,1)
%!   zeros(3) -t(2)*eye(3) eye(3) -t(4)*eye(3) zeros(3,1)
%!   ], tol);

%!test
%! assert(pt_model('2y'), [
%!    1 -1  0  0  0
%!    0  0  1 -1  0
%!   ]);
%! assert(pt_model('2y', 11, 21), [
%!    1  -1  0  0   0
%!    0 -11  1 -1  21
%!   ]);
%! assert(pt_model('2y', zeros(3)), [
%!   eye(3) -eye(3) zeros(3) zeros(3) zeros(3,1)
%!   zeros(3) zeros(3) eye(3) -eye(3) zeros(3,1)
%!   ]);
%! assert(pt_model('2y', magic(3), 11:13), [
%!   eye(3) -eye(3) zeros(3) zeros(3) zeros(3,1)
%!   zeros(3) -magic(3) eye(3) -eye(3) (11:13).'
%!   ]);
%! assert(pt_model('2y'), pt_model('2p'));
%! assert(pt_model('2y'), pt_model('2a'));
%! assert(pt_model('2y', 11), pt_model('2p', :, 11));
%! assert(pt_model('2y', :, 11), pt_model('2a', :, 11));
%! assert(pt_model('2y', zeros(3)), pt_model('2p', zeros(3), zeros(3)));
%! assert(pt_model('2y', zeros(3)), pt_model('2a', zeros(3,1), zeros(3,1)));
%! assert(pt_model('2y', magic(3)), pt_model('2p', zeros(3), magic(3)));
%! assert(pt_model('2y', zeros(3), 11:13), pt_model('2a', zeros(3,1), 11:13));

%!test
%! assert(pt_model('2z'), [
%!    1 -1  0  0  0
%!    0  0  1 -1  0
%!   ]);
%! assert(pt_model('2z', 11, 21), [
%!    1 -1  0 -11  21
%!    0  0  1  -1   0
%!   ]);
%! assert(pt_model('2z', zeros(3)), [
%!   eye(3) -eye(3) zeros(3) zeros(3) zeros(3,1)
%!   zeros(3) zeros(3) eye(3) -eye(3) zeros(3,1)
%!   ]);
%! assert(pt_model('2z', magic(3), 11:13), [
%!   eye(3) -eye(3) zeros(3) -magic(3) (11:13).'
%!   zeros(3) zeros(3) eye(3) -eye(3) zeros(3,1)
%!   ]);
%! assert(pt_model('2z'), pt_model('2p'));
%! assert(pt_model('2z'), pt_model('2a'));
%! assert(pt_model('2z', 11), pt_model('2p', 11));
%! assert(pt_model('2z', :, 11), pt_model('2a', 11));
%! assert(pt_model('2z', zeros(3)), pt_model('2p', zeros(3), zeros(3)));
%! assert(pt_model('2z', zeros(3)), pt_model('2a', zeros(3,1), zeros(3,1)));
%! assert(pt_model('2z', magic(3)), pt_model('2p', magic(3), zeros(3)));
%! assert(pt_model('2z', zeros(3), 11:13), pt_model('2a', 11:13, zeros(3,1)));

%!test
%! assert(pt_model('nb', 11), [0 11 0]);
%! assert(pt_model('nb', 11:12), [
%!   -1  1  0  0  0
%!    0  0 11 12  0
%!   ]);
%! assert(pt_model('nb', 11:13), [
%!   -1  1  0  0  0  0  0
%!    0 -1  1  0  0  0  0
%!    0  0  0 11 12 13  0
%!   ]);
%! assert(pt_model('nb', 11, 2), [
%!    0  0 11  0  0
%!    0  0  0 11  0
%!   ]);
%! assert(pt_model('nb', 11:12, 2), [
%!   -1  0  1  0  0  0  0  0  0
%!    0 -1  0  1  0  0  0  0  0
%!    0  0  0  0 11  0 12  0  0
%!    0  0  0  0  0 11  0 12  0
%!   ]);
%! assert(pt_model('nb', 11:13, 2), [
%!   -1  0  1  0  0  0  0  0  0  0  0  0  0
%!    0 -1  0  1  0  0  0  0  0  0  0  0  0
%!    0  0 -1  0  1  0  0  0  0  0  0  0  0
%!    0  0  0 -1  0  1  0  0  0  0  0  0  0
%!    0  0  0  0  0  0 11  0 12  0 13  0  0
%!    0  0  0  0  0  0  0 11  0 12  0 13  0
%!   ]);
