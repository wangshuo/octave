%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: PF = dc_sim (YZ, VS, IS, BC)
%%
%% Compute circuit responses for given stimuli.
%%
%% To consider the differential relations between the quantities,
%% nodes/branches are arranged as interlaced with integer locations.
%% Therefore, each type of node/branch pitch is 2 units,
%% which gives the relations between quantities and their density.
%%
%% A group of quantity value distribution is specified by a 3-D array
%% whose subscripts correspond to node/branch locations (X,Y,Z).
%%
%%   quantity type                   array subscripts
%%   --------------------------------------------------------
%%   electric node                   (  odd,  odd,  odd )
%%   electric branch toward x        ( even,  odd,  odd )
%%   electric branch toward y        (  odd, even,  odd )
%%   electric branch toward z        (  odd,  odd, even )
%%   magnetic node                   ( even, even, even )
%%   magnetic branch toward x        (  odd, even, even )
%%   magnetic branch toward y        ( even,  odd, even )
%%   magnetic branch toward z        ( even, even,  odd )
%%
%% YZ is branch admittance,
%% SX-by-SY-by-SZ complex array, where node values are ignored.
%%
%% VS and IS are branch voltage and current source,
%% SX-by-SY-by-SZ complex array, where node values are ignored.
%%
%% BC is boundary condition of 6 sides, [XL,XH,YL,YH,ZL,ZH],
%%   located on electric nodes for perfect electric conductor;
%%   located on magnetic nodes for perfect magnetic conductor;
%%   default is [1,SX,1,SY,1,SZ].
%% All quantity values equal zeros on the boundary.
%%
%% PF is node potential and branch motiveforce,
%% SX-by-SY-by-SZ complex array.
%% All other responses can be computed from this.
%%
%% See also: dc_simf, dc_simt.

function pf = dc_sim(yz, vs, is, bc=[ones(1,3);size(yz)])

  nc = numel(yz);
  sc = size(yz);
  assert(numel(sc) == 3 && sc >= 2, 'YZ must be 3-D array');
  assert(numel(bc) == 6 && 1 <= bc(1:2:end) && bc(1:2:end) <= bc(2:2:end) && bc(2:2:end) <= sc, 'BC must be [XL, XH, YL, YH, ZL, ZH]');

  %% electric/magnetic node/branch structure:
  %%           (z)
  %%           ne ---------bey---------ne
  %%          / |                     / |
  %%       bex  |      bmz         bex  |
  %%      /     |       :         /     |
  %%   ne ---------bey---------ne       |
  %%    |       |       :       |       |
  %%    |      bez      :  bmx  |      bez
  %%    |       |       : ;     |       |
  %%    |  bmy.........nm ......|..bmy  |
  %%    |       |     ; :       |       |
  %%   bez      |  bmx  :      bez      |
  %%    |       |       :       |       |
  %%    |      ne ------:--bey--|------ne (y)
  %%    |     /         :       |     /
  %%    |  bex         bmz      |  bex
  %%    | /                     | /
  %%   ne ---------bey---------ne
  %% (x)

  %% index to neighbor
  idx = sub2ind(sc, 2, 1, 1) - sub2ind(sc, 1, 1, 1);
  idy = sub2ind(sc, 1, 2, 1) - sub2ind(sc, 1, 1, 1);
  idz = sub2ind(sc, 1, 1, 2) - sub2ind(sc, 1, 1, 1);

  %% index of branch
  ibex = dc_ind(sc, 2:2:sc(1), 1:2:sc(2), 1:2:sc(3));
  ibey = dc_ind(sc, 1:2:sc(1), 2:2:sc(2), 1:2:sc(3));
  ibez = dc_ind(sc, 1:2:sc(1), 1:2:sc(2), 2:2:sc(3));
  ibmx = dc_ind(sc, 1:2:sc(1), 2:2:sc(2), 2:2:sc(3));
  ibmy = dc_ind(sc, 2:2:sc(1), 1:2:sc(2), 2:2:sc(3));
  ibmz = dc_ind(sc, 2:2:sc(1), 2:2:sc(2), 1:2:sc(3));

  %% index of node
  ine = dc_ind(sc, 1:2:sc(1), 1:2:sc(2), 1:2:sc(3));
  inm = dc_ind(sc, 2:2:sc(1), 2:2:sc(2), 2:2:sc(3));
  in = [ine; inm];

  %% index of boundary
  i0x = dc_ind(sc, [1:bc(1), bc(2):sc(1)], 1:sc(2), 1:sc(3));
  i0y = dc_ind(sc, 1:sc(1), [1:bc(3), bc(4):sc(2)], 1:sc(3));
  i0z = dc_ind(sc, 1:sc(1), 1:sc(2), [1:bc(5), bc(6):sc(3)]);
  i0 = [i0x; i0y; i0z];
  if (mod(bc, 2) == 1 || mod(bc, 2) == 0)  % prevent from floating
    i0(end+1) = sub2ind(sc, bc(1)+1, bc(3)+1, bc(5)+1);
    % i0(end+1) = sub2ind(sc, bc(2)-1, bc(4)-1, bc(6)-1);
  end

  %% physical laws:
  %%   Ohm's law:
  %%     ie = y * (ve + fe + ves) + ies
  %%     im = z * (vm + fm + vms) + ims
  %%   Kirchhoff's voltage law:
  %%     ve = -grad(pe)
  %%     vm = -grad(pm)
  %%   Kirchhoff's current law:
  %%     ie =  rot(fm) , div(fm) = 0
  %%     im = -rot(fe) , div(fe) = 0  (-1 for Lenz's law)
  %% algebra equations:
  %%   branch:
  %%     -y * fe + y * grad(pe) + rot(fm) = y * ves + ies
  %%     -z * fm + z * grad(pm) - rot(fe) = z * vms + ims
  %%   node:
  %%     div(fe) = 0
  %%     div(fm) = 0
  %%   boundary:
  %%     pe = 0 , fe = 0
  %%     pm = 0 , fm = 0

  AC = AR = (1:nc).'(:, ones(1,7));  % all
  AC(ibex, :) += [0, -idx,+idx, -idy,+idy, -idz,+idz];  % branch
  AC(ibey, :) += [0, -idy,+idy, -idz,+idz, -idx,+idx];  % branch
  AC(ibez, :) += [0, -idz,+idz, -idx,+idx, -idy,+idy];  % branch
  AC(ibmx, :) += [0, -idx,+idx, -idz,+idz, -idy,+idy];  % branch
  AC(ibmy, :) += [0, -idy,+idy, -idx,+idx, -idz,+idz];  % branch
  AC(ibmz, :) += [0, -idz,+idz, -idy,+idy, -idx,+idx];  % branch
  AC(in, :) += [0, -idx,+idx, -idy,+idy, -idz,+idz];  % node
  AC(i0, :) = AR(i0, :);  % boundary

  AV = [-yz(:), -yz(:),+yz(:), [-1,+1, +1,-1](ones(nc,1), :)];  % branch
  AV(in, :) = [0, -1,+1, -1,+1, -1,+1](ones(numel(in),1), :);  % node
  AV(i0, :) = 1;  % boundary

  pf = yz .* vs + is;  % branch
  pf(in) = 0;  % node
  pf(i0) = 0;  % boundary

  A = sparse(AR(:), AC(:), AV(:), nc, nc);
  pf(:) = A \ pf(:);

end

function ind = dc_ind(sc, x, y, z)

  [x, y, z] = ndgrid(x, y, z);
  ind = sub2ind(sc, x, y, z)(:);

end

%!xtest
%! tol = sqrt(eps);
%! SC = 3:6;
%! YZ = randn(SC([end,end,end])) + 1j * randn(SC([end,end,end]));
%! VS = randn(SC([end,end,end])) + 1j * randn(SC([end,end,end]));
%! IS = randn(SC([end,end,end])) + 1j * randn(SC([end,end,end]));
%!
%! sc = zeros(1,3); bc = zeros(1,6);
%! for sc(1) = SC for sc(2) = SC for sc(3) = SC
%!   for bc(1) = 1+(0:1) for bc(2) = sc(1)-(0:1)
%!   for bc(3) = 1+(0:1) for bc(4) = sc(2)-(0:1)
%!   for bc(5) = 1+(0:1) for bc(6) = sc(3)-(0:1)
%!     yz = YZ(1:sc(1), 1:sc(2), 1:sc(3));
%!     vs = VS(1:sc(1), 1:sc(2), 1:sc(3));
%!     is = IS(1:sc(1), 1:sc(2), 1:sc(3));
%!     pf = dc_sim(yz, vs, is, bc);
%!     assert(dc_div(pf, bc), zeros(sc), tol);
%!     v = dc_grad(pf, bc); i = dc_rot(pf, bc);
%!     err = (i - is) - yz .* (v + pf + vs);
%!     err(1:2:sc(1), 1:2:sc(2), 1:2:sc(3)) = 0;
%!     err(2:2:sc(1), 2:2:sc(2), 2:2:sc(3)) = 0;
%!     err([1:bc(1), bc(2):sc(1)], :, :) = 0;
%!     err(:, [1:bc(3), bc(4):sc(2)], :) = 0;
%!     err(:, :, [1:bc(5), bc(6):sc(3)]) = 0;
%!     assert(err, zeros(sc), tol);
%!     pf(bc(1)+1:bc(2)-1, bc(3)+1:bc(4)-1, bc(5)+1:bc(6)-1) = 0;
%!     assert(pf, zeros(sc), tol);
%!   end end end end end end
%! end end end
