%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: Q = dc_div (V, BC)
%%
%% Compute the divergence node values of branch values.
%%
%% V is branch values, SX-by-SY-by-SZ-by-... complex array,
%% where node values are ignored.
%%
%% BC is boundary condition of 6 sides, [XL,XH,YL,YH,ZL,ZH],
%%   located on electric nodes for perfect electric conductor;
%%   located on magnetic nodes for perfect magnetic conductor;
%%   default is [1,SX,1,SY,1,SZ].
%% All quantity values equal zeros on the boundary.
%%
%% Q is node values, SX-by-SY-by-SZ-by-... complex array,
%% where branch values are zeros.
%%
%% See also: dc_simf, dc_simt.

function q = dc_div(v, bc=[ones(1,3);size(v)(1:3)])

  sc = size(v);

  %% subscripts of node
  xne = 3:2:sc(1)-1; yne = 3:2:sc(2)-1; zne = 3:2:sc(3)-1;
  xnm = 2:2:sc(1)-1; ynm = 2:2:sc(2)-1; znm = 2:2:sc(3)-1;

  %% subscripts of boundary
  x0 = [1:bc(1), bc(2):sc(1)];
  y0 = [1:bc(3), bc(4):sc(2)];
  z0 = [1:bc(5), bc(6):sc(3)];

  v(x0, :, :, :) = v(:, y0, :, :) = v(:, :, z0, :) = 0;  % boundary
  q = zeros(sc);
  q(xne, yne, zne, :) = +v(xne+1, yne, zne, :) -v(xne-1, yne, zne, :)...
                        +v(xne, yne+1, zne, :) -v(xne, yne-1, zne, :)...
                        +v(xne, yne, zne+1, :) -v(xne, yne, zne-1, :);  % div
  q(xnm, ynm, znm, :) = +v(xnm+1, ynm, znm, :) -v(xnm-1, ynm, znm, :)...
                        +v(xnm, ynm+1, znm, :) -v(xnm, ynm-1, znm, :)...
                        +v(xnm, ynm, znm+1, :) -v(xnm, ynm, znm-1, :);  % div
  q(x0, :, :, :) = q(:, y0, :, :) = q(:, :, z0, :) = 0;  % boundary

end

%!xtest
%! tol = sqrt(eps);
%! SC = 3:6; NF = 1:3:4;
%! A = randn([SC([end,end,end]),NF(end)]);
%! B = randn([SC([end,end,end]),NF(end)]);
%!
%! sc = zeros(1,4); bc = zeros(1,6);
%! for sc(1) = SC for sc(2) = SC for sc(3) = SC for sc(4) = NF
%!   for bc(1) = 1+(0:1) for bc(2) = sc(1)-(0:1)
%!   for bc(3) = 1+(0:1) for bc(4) = sc(2)-(0:1)
%!   for bc(5) = 1+(0:1) for bc(6) = sc(3)-(0:1)
%!     a = A(1:sc(1), 1:sc(2), 1:sc(3), 1:sc(4));
%!     b = B(1:sc(1), 1:sc(2), 1:sc(3), 1:sc(4));
%!     assert(dc_div(a + b, bc), dc_div(a, bc) + dc_div(b, bc), tol);
%!     assert(dot(dc_grad(a, bc)(:), dc_rot(b, bc)(:)), 0, tol);
%!   end end end end end end
%! end end end end
