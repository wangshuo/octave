%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: PF = dc_simt (GR, CL, VS, IS, T, BC)
%%
%% Compute periodic-steady time domain circuit responses.
%%
%% To consider the differential relations between the quantities,
%% nodes/branches are arranged as interlaced with integer locations.
%% Therefore, each type of node/branch pitch is 2 units,
%% which gives the relations between quantities and their density.
%%
%% A group of quantity value distribution is specified by a 3-D array
%% whose subscripts correspond to node/branch locations (X,Y,Z).
%%
%%   quantity type                   array subscripts
%%   --------------------------------------------------------
%%   electric node                   (  odd,  odd,  odd )
%%   electric branch toward x        ( even,  odd,  odd )
%%   electric branch toward y        (  odd, even,  odd )
%%   electric branch toward z        (  odd,  odd, even )
%%   magnetic node                   ( even, even, even )
%%   magnetic branch toward x        (  odd, even, even )
%%   magnetic branch toward y        ( even,  odd, even )
%%   magnetic branch toward z        ( even, even,  odd )
%%
%% GR and CL are branch conductance and capacitance,
%% SX-by-SY-by-SZ real array, where node values are ignored.
%%
%% VS and IS are branch voltage and current source,
%% SX-by-SY-by-SZ-by-NT real array, where node values are ignored.
%% The 4th dimension can be omitted for time impulse source.
%%
%% T is time sweep, TSTART:TSTEP:TSTOP, NT-element real vector.
%%
%% BC is boundary condition of 6 sides, [XL,XH,YL,YH,ZL,ZH],
%%   located on electric nodes for perfect electric conductor;
%%   located on magnetic nodes for perfect magnetic conductor;
%%   default is [1,SX,1,SY,1,SZ].
%% All quantity values equal zeros on the boundary.
%%
%% PF is node potential and branch motiveforce,
%% SX-by-SY-by-SZ-by-NT real array.
%% All other responses can be computed from this.
%%
%% See also: dc_grad, dc_rot, dc_div.

function pf = dc_simt(gr, cl, vs, is, t, bc=[ones(1,3);size(gr+cl)])

  nt = numel(t);
  assert(nt > 1, 'T must be vector');
  tstep = t(2) - t(1);
  assert(range(diff(t(:))) < tstep * sqrt(eps), 'T must be range');
  tper = tstep * nt;

  f = (0 : nt/2) / tper;  % only use non-negative frequency part
  vs = dc_rfft(vs, nt, 4);
  is = dc_rfft(is, nt, 4);
  pf = dc_simf(gr, cl, vs, is, f, bc);
  pf = dc_irfft(pf, nt, 4);

end

function x = dc_rfft(x, n, dim)

  %% NOTE: fft() fails if dim > ndims(x)
  if (dim > ndims(x))
    x = postpad(x, 2, 0, dim);
  end

  %%      rfft(x)(1+k) = fft(x)(1+k)      for k = 0
  %% +--> rfft(x)(1+k) = fft(x)(1+k) * 2  for 0 < k < n/2
  %% |    rfft(x)(1+k) = fft(x)(1+k)      for k = n/2  if mod(n,2) = 0
  %% +--- rfft(x)(1+k) = []               for k > n/2
  x = fft(x, n, dim);
  x = postpad(x, floor(n/2+1.1), 0, dim);
  m = postpad(1, floor(n/2+0.9), 2, dim);
  m = postpad(m, floor(n/2+1.1), 1, dim);
  x = x .* m;

end

function x = dc_irfft(x, n, dim)

  %% NOTE: ifft() fails if dim > ndims(x)
  if (dim > ndims(x))
    x = postpad(x, 2, 0, dim);
  end

  x = real(ifft(x, n, dim));

end

%!demo
%! close all;
%! sc = [17, 13, 7];
%!
%! cl = zeros(sc);
%! cl([4:6,12:14], 2:12, 4) = cl(4:14, [2:4,10:12], 4) = 1e-3;  % iron
%! cl(1:2:end, 1:2:end, 4) = 0;  % iron
%! gr = 1e-9(ones(sc));
%! gr([4:6,12:14], 7, [3,5]) = gr([3,7,11,15], 7, 4) = 1;  % wire
%! gr([3,15], 7, 4) = 1e-3;  % terminals
%! t = 0:2e-9:2e-7-eps; vs = zeros([sc, numel(t)]);
%! vs(3, 7, 4, :) = 1 + 2 * sin(2*pi*1e+7*t);  % stimuli
%! figure; dc_plotm(cl / 1e-3); hold on; dc_plote(gr); dc_plote(vs(:,:,:,1));
%! legend('l(x,y,z)', 'g(x,y,z)', 'ves(x,y,z)');
%! title('iron transformer structure with voltage source');
%!
%! pf = dc_simt(gr, cl, vs, 0, t);
%! v = dc_grad(pf); i = dc_rot(pf);
%! figure;
%! subplot(211); plot(t, vs(3,7,4,:)(:), t, v(15,7,4,:)(:));
%! xlabel('t'); legend('ves(3,7,4,t)', 've(15,7,4,t)');
%! title('input/output voltage waveform');
%! subplot(212); plot(t, i(3,7,4,:)(:), t, i(15,7,4,:)(:));
%! xlabel('t'); legend('ie(3,7,4,t)', 'ie(15,7,4,t)');
%! title('input/output current waveform')

%!demo
%! close all;
%! sc = [47, 5, 5];
%! bc = [2,sc(1)-1, 1,sc(2), 1,sc(3)];
%!
%! cl = 1e-6(ones(sc));  % media
%! gr = 1e-9(ones(sc));
%! gr(4:2:44, 3, 3) = 1e+3;  % wire
%! gr([3,45], 3, 2) = 4;  % terminals
%! t = 0:1e-7:4e-5-eps; is = zeros([sc, numel(t)]);
%! is(3, 3, 2, :) = 2 * (t > 0.5e-5 & t <= 1.5e-5);  % stimuli
%! figure; dc_plote(gr / 1e+3); hold on; dc_plote(is(:,:,:,100));
%! legend('g(x)', 'ies(x)');
%! title('transmission line structure with current source');
%!
%! pf = dc_simt(gr, cl, 0, is, t, bc);
%! v = dc_grad(pf, bc); i = dc_rot(pf, bc);
%! figure; for n = 100:100:400 dc_plote(v(:,:,:,n)); hold on; end
%! legend('ve(x,1e-5)', 've(x,2e-5)', 've(x,3e-5)', 've(x,4e-5)');
%! title('voltage distribution at different time');
%! figure;
%! subplot(211); plot(t, squeeze(v(3:10:43,3,4,:)));
%! xlabel('t'); legend(num2str((3:10:43).', 've(%d,t)'));
%! title('voltage waveform at different location');
%! subplot(212); plot(t, squeeze(i(4:10:44,3,3,:)), t, is(3,3,2,:)(:));
%! xlabel('t'); legend([num2str((4:10:44).', 'ie(%d,t)'); 'ies(3,t)']);
%! title('current waveform at different location');
