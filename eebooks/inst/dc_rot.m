%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: I = dc_rot (F, BC)
%%
%% Compute the plus/minus curl branch values of branch values.
%% For example, to compute branch current from branch motiveforce.
%%
%% F is branch values, SX-by-SY-by-SZ-by-... complex array,
%% where node values are ignored.
%%
%% BC is boundary condition of 6 sides, [XL,XH,YL,YH,ZL,ZH],
%%   located on electric nodes for perfect electric conductor;
%%   located on magnetic nodes for perfect magnetic conductor;
%%   default is [1,SX,1,SY,1,SZ].
%% All quantity values equal zeros on the boundary.
%%
%% I is branch values, SX-by-SY-by-SZ-by-... complex array,
%% where node values are zeros.
%%
%% See also: dc_simf, dc_simt.

function i = dc_rot(f, bc=[ones(1,3);size(f)(1:3)])

  sc = size(f);

  %% subscripts of branch
  xbex = 2:2:sc(1)-1; ybex = 3:2:sc(2)-1; zbex = 3:2:sc(3)-1;
  xbey = 3:2:sc(1)-1; ybey = 2:2:sc(2)-1; zbey = 3:2:sc(3)-1;
  xbez = 3:2:sc(1)-1; ybez = 3:2:sc(2)-1; zbez = 2:2:sc(3)-1;
  xbmx = 3:2:sc(1)-1; ybmx = 2:2:sc(2)-1; zbmx = 2:2:sc(3)-1;
  xbmy = 2:2:sc(1)-1; ybmy = 3:2:sc(2)-1; zbmy = 2:2:sc(3)-1;
  xbmz = 2:2:sc(1)-1; ybmz = 2:2:sc(2)-1; zbmz = 3:2:sc(3)-1;

  %% subscripts of boundary
  x0 = [1:bc(1), bc(2):sc(1)];
  y0 = [1:bc(3), bc(4):sc(2)];
  z0 = [1:bc(5), bc(6):sc(3)];

  f(x0, :, :, :) = f(:, y0, :, :) = f(:, :, z0, :) = 0;  % boundary
  i = zeros(sc);
  i(xbex, ybex, zbex, :) = +f(xbex, ybex+1, zbex, :) -f(xbex, ybex-1, zbex, :) -f(xbex, ybex, zbex+1, :) +f(xbex, ybex, zbex-1, :);  %  rot
  i(xbey, ybey, zbey, :) = +f(xbey, ybey, zbey+1, :) -f(xbey, ybey, zbey-1, :) -f(xbey+1, ybey, zbey, :) +f(xbey-1, ybey, zbey, :);  %  rot
  i(xbez, ybez, zbez, :) = +f(xbez+1, ybez, zbez, :) -f(xbez-1, ybez, zbez, :) -f(xbez, ybez+1, zbez, :) +f(xbez, ybez-1, zbez, :);  %  rot
  i(xbmx, ybmx, zbmx, :) = -f(xbmx, ybmx+1, zbmx, :) +f(xbmx, ybmx-1, zbmx, :) +f(xbmx, ybmx, zbmx+1, :) -f(xbmx, ybmx, zbmx-1, :);  % -rot
  i(xbmy, ybmy, zbmy, :) = -f(xbmy, ybmy, zbmy+1, :) +f(xbmy, ybmy, zbmy-1, :) +f(xbmy+1, ybmy, zbmy, :) -f(xbmy-1, ybmy, zbmy, :);  % -rot
  i(xbmz, ybmz, zbmz, :) = -f(xbmz+1, ybmz, zbmz, :) +f(xbmz-1, ybmz, zbmz, :) +f(xbmz, ybmz+1, zbmz, :) -f(xbmz, ybmz-1, zbmz, :);  % -rot
  i(x0, :, :, :) = i(:, y0, :, :) = i(:, :, z0, :) = 0;  % boundary

end

%!xtest
%! tol = sqrt(eps);
%! SC = 3:6; NF = 1:3:4;
%! A = randn([SC([end,end,end]),NF(end)]);
%! B = randn([SC([end,end,end]),NF(end)]);
%!
%! sc = zeros(1,4); bc = zeros(1,6);
%! for sc(1) = SC for sc(2) = SC for sc(3) = SC for sc(4) = NF
%!   for bc(1) = 1+(0:1) for bc(2) = sc(1)-(0:1)
%!   for bc(3) = 1+(0:1) for bc(4) = sc(2)-(0:1)
%!   for bc(5) = 1+(0:1) for bc(6) = sc(3)-(0:1)
%!     a = A(1:sc(1), 1:sc(2), 1:sc(3), 1:sc(4));
%!     b = B(1:sc(1), 1:sc(2), 1:sc(3), 1:sc(4));
%!     assert(dc_rot(a + b, bc), dc_rot(a, bc) + dc_rot(b, bc), tol);
%!     assert(dc_div(dc_rot(a, bc), bc), zeros(sc), tol);
%!   end end end end end end
%! end end end end
