%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: MAPPER = as_matz (PORTMAP)
%%
%% Generate port mapper to compress matrix.
%%
%% PORTMAP specifies location I belongs to port PORTMAP(I),
%% NR-element vector, the set of which must be 1:NP.
%%
%% MAPPER is port mapper, NR-by-NP bool matrix.
%% For matrix MAT, use (MAT * MAPPER) to compress source,
%% and (MAPPER.' * MAT) to compress destination.
%%
%% See also: as_matf.

function mapper = as_matz(portmap)

  mapper = eye(numel(unique(portmap)), 'logical')(portmap, :);

end

%!test
%! for n = 1:4 for r = 0:5
%!   p = [1:n randi(n, 1,r)](randperm(end));
%!   m = as_matz(p);
%!   assert(islogical(m));
%!   assert(sum(m, 2) == 1);
%!   assert((1:columns(m)) * m.', p);
%! end end
