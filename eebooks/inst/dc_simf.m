%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: PF = dc_simf (GR, CL, VS, IS, F, BC)
%%
%% Compute frequency domain circuit responses.
%%
%% To consider the differential relations between the quantities,
%% nodes/branches are arranged as interlaced with integer locations.
%% Therefore, each type of node/branch pitch is 2 units,
%% which gives the relations between quantities and their density.
%%
%% A group of quantity value distribution is specified by a 3-D array
%% whose subscripts correspond to node/branch locations (X,Y,Z).
%%
%%   quantity type                   array subscripts
%%   --------------------------------------------------------
%%   electric node                   (  odd,  odd,  odd )
%%   electric branch toward x        ( even,  odd,  odd )
%%   electric branch toward y        (  odd, even,  odd )
%%   electric branch toward z        (  odd,  odd, even )
%%   magnetic node                   ( even, even, even )
%%   magnetic branch toward x        (  odd, even, even )
%%   magnetic branch toward y        ( even,  odd, even )
%%   magnetic branch toward z        ( even, even,  odd )
%%
%% GR and CL are branch conductance and capacitance,
%% SX-by-SY-by-SZ real array, where node values are ignored.
%%
%% VS and IS are branch voltage and current source,
%% SX-by-SY-by-SZ-by-NF complex array, where node values are ignored.
%% The 4th dimension can be omitted for frequency constant source.
%%
%% F is frequency sweep, NF-element real vector.
%%
%% BC is boundary condition of 6 sides, [XL,XH,YL,YH,ZL,ZH],
%%   located on electric nodes for perfect electric conductor;
%%   located on magnetic nodes for perfect magnetic conductor;
%%   default is [1,SX,1,SY,1,SZ].
%% All quantity values equal zeros on the boundary.
%%
%% PF is node potential and branch motiveforce,
%% SX-by-SY-by-SZ-by-NF complex array.
%% All other responses can be computed from this.
%%
%% See also: dc_grad, dc_rot, dc_div.

function pf = dc_simf(gr, cl, vs, is, f, bc=[ones(1,3);size(gr+cl)])

  sc = size(gr + cl);
  nf = numel(f);

  pf = zeros([sc, nf]);
  for i = 1:nf
    pf(:,:,:,i) = dc_sim(gr + 2j*pi*f(i) * cl,
                         vs(:,:,:, ifelse(end>1, i, 1)),
                         is(:,:,:, ifelse(end>1, i, 1)),
                         bc);
  end

end

%!demo
%! close all;
%! sc = [21, 21, 5];
%! bc = [2,sc(1)-1, 2,sc(2)-1, 2,sc(3)-1];
%!
%! gr = 1e-9(ones(sc));
%! gr(8:2:14, 7:2:15, 3) = gr(7:2:15, 8:2:14, 3) = 1e-3;  % resistor
%! gr(6:2:14, [7,15], 3) = gr(5, 8:2:14, 3) = 1e+3;  % terminals
%! vs = zeros(sc);
%! vs(5, 8:2:14, 3) = 1/4;  % stimuli
%! figure; dc_plote(min(2, gr / 1e-3)); hold on; dc_plote(vs);
%! legend('g(x,y)', 'ves(x,y)');
%! title('sheet resistor structure with voltage source');
%!
%! pf = dc_simf(gr, 0, vs, 0, 0, bc);
%! v = dc_grad(pf, bc); i = dc_rot(pf, bc);
%! figure; dc_plote(v / max(abs(v(:))), 1); legend('ve(x,y)');
%! title('voltage distribution');
%! figure; dc_plote(i / 1e-3); legend('ie(x,y)');
%! title('current distribution');
%! figure; barh([i(5,8,3) 5/4*1e-3; nan nan]); xlabel('ie(5,8)');
%! legend('simulated', 'theoretic'); yticks([]);
%! title('source current');

%!demo
%! close all;
%! sc = 13(ones(1,3));
%!
%! cl = 1e-9(ones(sc));
%! cl(4:2:10, 5:2:9, 5:2:9) = 1e-6;  % capacitor
%! cl(3:2:11, 6:2:8, 5:2:9) = 1e-6;  % capacitor
%! cl(3:2:11, 5:2:9, 6:2:8) = 1e-6;  % capacitor
%! gr = vs = zeros(sc);
%! gr(4:2:10, 3:2:9, [5,9]) = 1;  % terminals
%! gr(3:2:11, 4:2:8, [5,9]) = 1;  % terminals
%! gr(7, 3, 6:2:8) = 1;  % terminals
%! f = logspace(0, 6, 31); vs(7, 3, 6:2:8) = 1/2;  % stimuli
%! figure; dc_plote(cl / 1e-6); hold on; dc_plote(gr); dc_plote(vs);
%! legend('c(x,y,z)', 'g(x,y,z)', 'ves(x,y,z)');
%! title('plate capacitor structure with voltage source');
%!
%! pf = dc_simf(gr, cl, vs, 0, f);
%! v = dc_grad(pf); i = dc_rot(pf);
%! v1 = real(v(:,:,:,1)); i1 = imag(i(:,:,:,1));
%! figure; dc_plote(v1 / max(abs(v1(:))), 1); legend('ver(x,y,z,1)');
%! title('voltage distribution at DC');
%! figure; dc_plote(i1 / 25e-6); legend('iei(x,y,z,1)');
%! title('current distribution at DC');
%! c0 = 5*3/2*1e-6; i0 = 2j*pi*f*c0; i1 = i(7,3,6,:)(:);
%! figure;
%! subplot(212); semilogx(f, unwrap(arg(i1)), 'x', f, unwrap(arg(i0)));
%! legend('simulated', 'theoretic'); xlabel('f'); ylabel('iep(7,3,6,f)');
%! subplot(211); loglog(f, abs(i1), 'x', f, abs(i0));
%! legend('simulated', 'theoretic'); xlabel('f'); ylabel('iem(7,3,6,f)');
%! title('source current Bode-plot');

%!demo
%! close all;
%! sc = 13(ones(1,3));
%! bc = [1,sc(1), 1,sc(2), 2,sc(3)-1];
%!
%! cl = 1e-9(ones(sc));
%! cl(1:2:end, 2:2:end, 2:2:end) = 1e-3;  % media
%! cl(2:2:end, 1:2:end, 2:2:end) = 1e-3;  % media
%! cl(2:2:end, 2:2:end, 1:2:end) = 1e-3;  % media
%! gr = is = zeros(sc);
%! gr([3,11], 6:2:8, 3:2:11) = gr([3,11], 5:2:9, 4:2:10) = 1;  % wire
%! gr(4:2:10, [5,9], 3:2:11) = gr(3:2:11, [5,9], 4:2:10) = 1;  % wire
%! gr(4, 5, 3:2:11) = 0;  % wire
%! f = logspace(0, 6, 31); is(4, 5, 3:2:11) = -1/5;  % stimuli
%! figure; dc_plote(gr); hold on; dc_plote(is);
%! legend('g(x,y,z)', 'ies(x,y,z)');
%! title('cylinder inductor structure with current source');
%!
%! pf = dc_simf(gr, cl, 0, is, f, bc);
%! v = dc_grad(pf, bc); i = dc_rot(pf, bc);
%! ir = real(i(:,:,:,1)); ii = imag(i(:,:,:,1));
%! figure; dc_plote(ir * 5, 1); legend('ier(x,y,z,1)');
%! title('electric current distribution at DC');
%! figure; dc_plotm(ii / 1e-3, 1); legend('imi(x,y,z,1)');
%! title('magnetic current distribution at DC');
%! r0 = 11/5/1; l0 = 4*2/6*1e-3; v0 = r0 + 2j*pi*f*l0; v1 = v(4,5,7,:)(:);
%! figure;
%! subplot(212); loglog(f, imag(v1), 'x', f, imag(v0));
%! legend('simulated', 'theoretic'); xlabel('f'); ylabel('vei(4,5,7,f)');
%! subplot(211); loglog(f, real(v1), 'x', f, real(v0));
%! legend('simulated', 'theoretic'); xlabel('f'); ylabel('ver(4,5,7,f)');
%! title('source voltage Bode-plot');
