%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: [V, I] = pt_ac (
%%                   PORTMAP1, MODEL1,
%%                   PORTMAP2, MODEL2,
%%                   ...,
%%                 END)
%%
%% AC analyse circuit with instances of model or subckt.
%% The portmap of each MODEL is specified by vector PORTMAP,
%% and the set of all PORTMAP must be 1:NC.
%% V and I are circuit voltage and current, NC-element column vectors.
%% The trailing END is optional.
%%
%% See also: pt_model, pt_subckt.

function [V, I] = pt_ac(varargin)
  ckt = pt_xx([], varargin{:});
  A = ckt(:,1:end-1);
  B = ckt(:,end);
  assert(issquare(A));
  X = A \ B;
  V = X(1:end/2,:);
  I = X(end/2+1:end,:);
end

%!demo
%! close all;
%! tol = eps^0.5;
%! vi = 10; r = 50; l = 0.5; c = 20e-9;
%! w = logspace(0, 8);
%! vo = []; for s = 1j*w
%!   [V, I] = pt_ac(
%!     [1    ], pt_model('1i', vi/r, 1/r),
%!     [1 3 2], pt_model('nb', [1 -1 -1]),
%!     [2    ], pt_model('1z', s*l),
%!     [3    ], pt_model('1y', s*c),
%!     'end');
%! vo(end+1) = V(1); end
%! assert(vo/vi, 1./(1 + r./(1j*w*l) + r.*(1j*w*c)), tol);
%! figure;
%! subplot(212); semilogx(w, arg(vo/vi)); xlabel('\omega'); ylabel('\phi_H');
%! subplot(211); loglog(w, abs(vo/vi)); xlabel('\omega'); ylabel('|H|');
%! title('Bode plot of RLC tank');

%!demo
%! close all;
%! tol = eps^0.5;
%! vi = 10; r = 50; z = 50e-3j; y = 20e-6j;
%! l = linspace(0, 2e+3*pi);
%! vo = io = []; for len = l
%!   [V, I] = pt_ac(
%!     [1  ], pt_model('1v', vi),
%!     [1 2], pt_model('2p', z*len, y*len),
%!     [2  ], pt_model('1z', r),
%!     [3  ], pt_model('1v', vi, r),
%!     [3 4], pt_model('2p', z*len, y*len),
%!     [4  ], pt_model('1y'),
%!     'end');
%! vo(:,end+1) = V; io(:,end+1) = I; end
%! gamma = sqrt(z * y);
%! [a, b] = pt_zct(vo, io);
%! assert((b./a)(1,:), zeros(size(l)), tol);
%! assert((b./a)(2,:), zeros(size(l)), tol);
%! assert((b./a)(3,:), exp(-2*gamma*l), tol);
%! assert((b./a)(4,:), ones(size(l)), tol);
%! assert(vo(1,:)./vi, ones(size(l)), tol);
%! assert(vo(2,:)./vi, exp(-gamma*l), tol);
%! figure; plot3(l, (b./a)(1,:), ';port 1;', l, (b./a)(3,:), ';port 3;');
%! ylim([-1 1]); zlim([-1 1]);
%! xlabel('length'); ylabel('Re \Gamma'); zlabel('Im \Gamma');
%! title('Smith chart of transmission line');
%! figure; plot3(l, vo(1,:)./vi, ';port 1;', l, vo(2,:)./vi, ';port 2;');
%! ylim([-1 1]); zlim([-1 1]);
%! xlabel('length'); ylabel('Re H'); zlabel('Im H');
%! title('Smith chart of transmission line');

%!demo
%! close all;
%! tol = eps^0.5;
%! r = 25; g = 1/100; ro = 50;
%!   my_subckt = pt_subckt([5 6 1 2],
%!     [5 6 3 4], pt_model('2z', r*ones(2)),
%!     [3 4 1 2], pt_model('2y', g*[1 -1;-1 1]),
%!     'end');
%! ii = []; for vi = pt_isct(eye(2))
%!   [V, I] = pt_ac(
%!     [1 2    ], pt_model('1v', vi),
%!     [1 2 3 4], my_subckt,
%!     [    3 4], pt_model('1z', ro*eye(2)),
%!     'end');
%! ii(:,end+1) = pt_sct(I(1:2)); end
%! gc = 1/(r*2 + ro);
%! gd = g*2 + 1/ro;
%! assert(ii, diag([gc gd]), tol);
%! msgbox('mode analysis of differential circuit');
