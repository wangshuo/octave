%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: H = dc_plotm (B)
%% usage: H = dc_plotm (B, VEC, STYLE)
%%
%% Plot magnetic branch value distribution.
%%
%% B is magnetic branch values, SX-by-SY-by-SZ real array,
%% where other type values are ignored.
%%
%% VEC is true to plot as vector-on-node format;
%% default is to plot as original branch format.
%%
%% STYLE is the style to use for the plot.
%%
%% H is the optional graphics handle to the created plot.
%%
%% See also: dc_simf, dc_simt.

function h = dc_plotm(b, vec=[], varargin)

  sc = size(b);

  %% subscripts of node
  xnm = 2:2:sc(1)-1; ynm = 2:2:sc(2)-1; znm = 2:2:sc(3)-1;

  %% subscripts of branch
  xbmx = 3:2:sc(1)-1; ybmx = 2:2:sc(2)-1; zbmx = 2:2:sc(3)-1;
  xbmy = 2:2:sc(1)-1; ybmy = 3:2:sc(2)-1; zbmy = 2:2:sc(3)-1;
  xbmz = 2:2:sc(1)-1; ybmz = 2:2:sc(2)-1; zbmz = 3:2:sc(3)-1;

  if (vec)
    R = dc_xyz(xnm, ynm, znm);
    Y = [(b(xnm+1, ynm, znm)(:) + b(xnm-1, ynm, znm)(:)) / 2,...
         (b(xnm, ynm+1, znm)(:) + b(xnm, ynm-1, znm)(:)) / 2,...
         (b(xnm, ynm, znm+1)(:) + b(xnm, ynm, znm-1)(:)) / 2];
  else
    R = [dc_xyz(xbmx, ybmx, zbmx)
         dc_xyz(xbmy, ybmy, zbmy)
         dc_xyz(xbmz, ybmz, zbmz)];
    Y = blkdiag(b(xbmx, ybmx, zbmx)(:),
                b(xbmy, ybmy, zbmy)(:),
                b(xbmz, ybmz, zbmz)(:));
  end
  h = dc_ploty(R, Y, varargin{:});

end

function xyz = dc_xyz(x, y, z)

  [x, y, z] = ndgrid(x, y, z);
  xyz = [x(:), y(:), z(:)];

end
