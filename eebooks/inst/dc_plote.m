%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: H = dc_plote (B)
%% usage: H = dc_plote (B, VEC, STYLE)
%%
%% Plot electric branch value distribution.
%%
%% B is electric branch values, SX-by-SY-by-SZ real array,
%% where other type values are ignored.
%%
%% VEC is true to plot as vector-on-node format;
%% default is to plot as original branch format.
%%
%% STYLE is the style to use for the plot.
%%
%% H is the optional graphics handle to the created plot.
%%
%% See also: dc_simf, dc_simt.

function h = dc_plote(b, vec=[], varargin)

  sc = size(b);

  %% subscripts of node
  xne = 3:2:sc(1)-1; yne = 3:2:sc(2)-1; zne = 3:2:sc(3)-1;

  %% subscripts of branch
  xbex = 2:2:sc(1)-1; ybex = 3:2:sc(2)-1; zbex = 3:2:sc(3)-1;
  xbey = 3:2:sc(1)-1; ybey = 2:2:sc(2)-1; zbey = 3:2:sc(3)-1;
  xbez = 3:2:sc(1)-1; ybez = 3:2:sc(2)-1; zbez = 2:2:sc(3)-1;

  if (vec)
    R = dc_xyz(xne, yne, zne);
    Y = [(b(xne+1, yne, zne)(:) + b(xne-1, yne, zne)(:)) / 2,...
         (b(xne, yne+1, zne)(:) + b(xne, yne-1, zne)(:)) / 2,...
         (b(xne, yne, zne+1)(:) + b(xne, yne, zne-1)(:)) / 2];
  else
    R = [dc_xyz(xbex, ybex, zbex)
         dc_xyz(xbey, ybey, zbey)
         dc_xyz(xbez, ybez, zbez)];
    Y = blkdiag(b(xbex, ybex, zbex)(:),
                b(xbey, ybey, zbey)(:),
                b(xbez, ybez, zbez)(:));
  end
  h = dc_ploty(R, Y, varargin{:});

end

function xyz = dc_xyz(x, y, z)

  [x, y, z] = ndgrid(x, y, z);
  xyz = [x(:), y(:), z(:)];

end
