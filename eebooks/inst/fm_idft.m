%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: [XT, T] = fm_idft (XF, F)
%% usage: [XT, T] = fm_idft (XF, F, DIM)
%%
%% Compute the inverse discrete-time Fourier transform.
%%
%% See also: fm_dft.

function [x, t] = fm_idft(x, f, varargin)
  [t, n, fp, fs] = fm_tfparams(f);
  x = fm_isfft(x, n, varargin{:});
end

%!test
%! tol = eps^0.5;
%! t1 = -5:0.1:5;
%! x1 = exp(-t1.^2);
%! t2 = t1(1:end-1);
%! x2 = x1(1:end-1);
%! [x_, t_] =  fm_dft(x1, t1);
%! [x_, t_] = fm_idft(x_, t_);
%! assert(x_, x1, tol);
%! assert(t_, t1, tol);
%! [x_, t_] = fm_idft(x1, t1);
%! [x_, t_] =  fm_dft(x_, t_);
%! assert(x_, x1, tol);
%! assert(t_, t1, tol);
%! [x_, t_] =  fm_dft(x2, t2);
%! [x_, t_] = fm_idft(x_, t_);
%! assert(x_, x2, tol);
%! assert(t_, t2, tol);
%! [x_, t_] = fm_idft(x2, t2);
%! [x_, t_] =  fm_dft(x_, t_);
%! assert(x_, x2, tol);
%! assert(t_, t2, tol);
