%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: [ZC, GAMMA] = pt_zc (Z, Y)
%% usage: [ZC, INV_U] = pt_zc (L, C)
%%
%% Compute characteristic of transmission line.
%% Z is impedance of unit length, NL-by-NL matrix.
%% Y is admittance of unit length, NL-by-NL matrix.
%% ZC is wave mode, (NL*2)-by-(NL*2) matrix with columns [V;I] where
%% V and I are wave mode voltage and current, NL-element column vectors.
%% GAMMA is propagation constant, (NL*2)-by-(NL*2) diagonal matrix.
%%
%% If invoked with L and C, inductance and capacitance of unit length,
%% then return INV_U, reciprocal wave velocity.
%%
%% See also: pt_zct, pt_izct.

function [ZC, GAMMA] = pt_zc(Z=50, Y=inv(Z))
  assert(issquare(Z));
  assert(issquare(Y));
  assert(length(Z), length(Y));
  [VC, GC] = eig(Z * Y); GC = sqrt(GC);
  IC = Y * VC / GC;
  normalizer = sqrt(abs(diag(dot(IC, VC))));
  VC = VC / normalizer;
  IC = IC / normalizer;
  ZC = [VC VC; IC -IC];
  GC = diag(GC); GAMMA = diag([GC; -GC]);
end

%!shared tol
%! tol = eps^0.5;
%!function check(tol, z, y)
%! [zc, gamma] = pt_zc(z, y);
%! c = [zeros(size(z)) z; y zeros(size(y))];
%! assert(c*zc, zc*gamma, tol);
%! vc = zc(1:end/2,:);
%! ic = zc(end/2+1:end,:);
%! assert(abs(diag(ic'*vc)), diag(eye(size(zc))), tol);  % not .'
%!endfunction

%!test
%! [zc, gamma] = pt_zc();
%! assert(zc, [sqrt(50) sqrt(50);sqrt(1/50) -sqrt(1/50)], tol);
%! assert(gamma, diag([1 -1]), tol);
%! [zc, gamma] = pt_zc(5, 80);
%! assert(zc, [1/2 1/2;2 -2], tol);
%! assert(gamma, diag([20 -20]), tol);

%!test
%! check(tol, diag(11:12), diag(21:22));
%! check(tol, [11 2;2 11], [21 3;3 21]);
%! check(tol, [12 11;11 11], [21 -21;-21 23]);
%! check(tol, diag(11:13), magic(3));
