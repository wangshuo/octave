%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: SUBCKT = pt_subckt (PORT,
%%                   PORTMAP1, MODEL1,
%%                   PORTMAP2, MODEL2,
%%                   ...,
%%                 ENDS)
%%
%% Define subckt with instances of model or subckt.
%% The portmap of each MODEL is specified by vector PORTMAP,
%% and the set of all PORTMAP must be 1:NC.
%% The port of subckt is specified by vector PORT.
%% The trailing ENDS is optional.
%%
%% See also: pt_model, pt_ac.

function subckt = pt_subckt(port, varargin)
  ckt = pt_xx(port, varargin{:});
  A = ckt(:,1:end-1);
  B = ckt(:,end);
  nc = columns(A)/2;
  np = numel(port);
  intr = setdiff(1:nc, port);
  demapper = eye(nc*2)(:, [intr intr+nc port port+nc]);
  A = A * demapper;
  [~, ckt] = lu([A B]);
  subckt = ckt(end-np+1:end, end-np*2:end);
end

%!function check(tol, varargin)
%! varargin = reshape(varargin, 2,[]);
%! [v,i] = pt_ac(varargin{:});
%! for n = 1:columns(varargin) [p,m] = varargin{:,n};
%!   assert(m(:,1:end-1) * [v(p);i(p)], m(:,end), tol);
%!   s = pt_subckt(p, varargin{:,[1:n-1 n+1:end]});
%!   [v_,i_] = pt_ac(1:numel(p), m, 1:numel(p), s);
%!   assert(v_, v(p), tol); assert(i_, i(p), tol);
%! end
%!endfunction

%!xtest
%! tol = eps^0.5;
%! randsplit = @(n) mat2cell(randperm(sum(n)), 1,n);
%! for n = {{1},{[1 1],2},{[1 1 1],[1 2],3},{[1 1 1 1],[1 1 2],[1 3],[2 2],4}}
%!   n = n{1}; for n1 = n n1 = n1{1}; for n2 = n n2 = n2{1};
%!     p = [randsplit(n1) randsplit(n2)](randperm(end));
%!     m = cellfun(@(x)randn(numel(x),numel(x)*2+1), p, 'UniformOutput',false);
%!     check(tol, [p;m]{:});
%!   end end
%! end
