%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: H = as_plotd (R, VAL)
%% usage: H = as_plotd (R, VAL, DEG, STYLE)
%%
%% Plot value distribution, polarization map or radiation pattern.
%%
%% R is locations, ND-by-3 real matrix.
%%
%% VAL is values,
%%   ND-by-3 real matrix for distribution,
%%   ND-by-3 complex matrix for polarization map,
%%   ND-by-1 real matrix for radiation pattern.
%%
%% DEG is degrees for polarization map, or empty (default).
%%
%% STYLE is the style to use for the plot.
%%
%% H is the optional graphics handle to the created plot.
%%
%% See also: as_matf, as_setd.

function h = as_plotd(r, val, deg=[], varargin)

  if (numel(deg))
    nd = rows(val); np = numel(deg);
    r = repmat(r, np,1);
    val = repmat(val, np,1);
    deg = repelem(deg(:), nd,1);
    val = real(val .* exp(1j*deg2rad(deg)));
  end

  switch (columns(val))
  case 1
    [x,y,z] = as_node(r .* val);
    h = plot3(x,y,z, '.',varargin{:});
  case 3
    [x,y,z] = as_node(r - val/2);
    [u,v,w] = as_node(val);
    h = quiver3(x,y,z, u,v,w, 0,varargin{:});
  end
  xlabel('x'); ylabel('y'); zlabel('z');

end
