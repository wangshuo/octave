%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: P = pt_isct (S)
%%
%% Compute the inverse symmetrical component transform.
%%
%% See also: pt_sct.

function P = pt_isct(S, F=fft(eye(rows(S))))
  assert(issquare(F));
  assert(rows(S), length(F));
  P = F * S;
end

%!test
%! tol = eps^0.5;
%! assert(pt_isct(11), 11);
%! assert(pt_isct((11:12).'), fft((11:12).'), tol);
%! assert(pt_isct((11:13).'), fft((11:13).'), tol);
