%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: S = pt_sct (P)
%%
%% Compute the symmetrical component transform.
%%
%% See also: pt_isct.

function S = pt_sct(P, F=fft(eye(rows(P))))
  assert(issquare(F));
  assert(rows(P), length(F));
  S = F \ P;
end

%!test
%! tol = eps^0.5;
%! assert(pt_sct(11), 11);
%! assert(pt_sct((11:12).'), ifft((11:12).'), tol);
%! assert(pt_sct((11:13).'), ifft((11:13).'), tol);
