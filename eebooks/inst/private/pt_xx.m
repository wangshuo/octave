%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: pt_xx (PORT, PORTMAP1, MODEL1, PORTMAP2, MODEL2, ..., ENDX)
%%
%% Undocumented internal function.

function ckt = pt_xx(port, varargin)
  assert(numel(unique(port)), numel(port));
  varargin = varargin(1:floor(end/2)*2);
  varargin = reshape(varargin, 2,[]);
  port = [port varargin{1,:}];
  assert(isvector(port)); port = port(:).';
  nc = max(port);
  assert(sort(port), sort([1:nc 1:nc]));
  ckt = []; for inst = varargin
    ckt = [ckt; pt_x(nc, inst{:})];
  end
end

function inst = pt_x(nc, portmap, model)
  assert(isscalar(nc));
  assert(isvector(portmap)); portmap = portmap(:).';
  assert(numel(unique(portmap)), numel(portmap));
  A = model(:,1:end-1);
  B = model(:,end);
  assert(issquare([A; A]));
  assert(numel(portmap), columns(A)/2);
  mapper = eye(nc*2)([portmap portmap+nc], :);
  A = A * mapper;
  inst = [A B];
end
