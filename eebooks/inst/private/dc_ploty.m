%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: dc_ploty (R, Y, STYLE)
%%
%% Undocumented internal function.

function h = dc_ploty(R, Y, varargin)

  R = R - Y/2;
  h = quiver3(num2cell(R,1){:}, num2cell(Y,1){:}, 0,varargin{:});
  xlabel('x'); ylabel('y'); zlabel('z');

end
