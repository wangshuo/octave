%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: [R1, R2, R3, R4] = as_loop (R)
%%
%% Undocumented internal function.

function [r1,r2,r3,r4] = as_loop(r)
  [r1,r2,r3,r4] = deal(mat2cell(r, rows(r), [3 3 3 3]){:});
  [r1,r2,r3,r4] = {[r1 r2],[r2 r3],[r3 r4],[r4 r1]}{:};
end
