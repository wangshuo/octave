%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: [F, N, TS, TP] = fm_tfparams (T)
%% usage: [T, N, FP, FS] = fm_tfparams (F)
%%
%% Undocumented internal function.

function [f, n, ts, tp] = fm_tfparams(t)
  name = toupper(inputname(1));
  n = numel(t);
  assert(n > 1, '%s must be vector', name);
  ts = t(2) - t(1);
  assert(range(diff(t)) < ts*eps^0.5, '%s must be range', name);
  tp = ts * n;
  assert(abs(ifftshift(t)(1)) < ts*eps^0.5, '%s must be symmetric', name);
  f = t / (ts * tp);
end
