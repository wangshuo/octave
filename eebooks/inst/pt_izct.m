%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: [V, I] = pt_izct (A, B, ZC)
%%
%% Compute the inverse travelling wave transform.
%% ZC is wave mode; default is of 50-Ohm uncoupled transmission line.
%%
%% See also: pt_zct, pt_zc.

function [V, I] = pt_izct(A, B, ZC=pt_zc(50*eye(rows(A))))
  assert(size(A), size(B));
  assert(issquare(ZC));
  assert(rows([A; B]), length(ZC));
  X = [A; B];
  X = ZC * X;
  V = X(1:end/2,:);
  I = X(end/2+1:end,:);
end

%!test
%! tol = eps^0.5;
%! [v, i] = pt_izct(2, 1);
%! assert(v, sqrt(50)*3, tol);
%! assert(i, sqrt(1/50), tol);
%! [v, i] = pt_izct(2, 1, [2 3;5 7]);
%! assert(v, 7, tol);
%! assert(i, 17, tol);
%! [v, i] = pt_izct([1;3;5], [1;2;3]);
%! assert(v, sqrt(50)*[2;5;8], tol);
%! assert(i, sqrt(1/50)*[0;1;2], tol);
%! [v, i] = pt_izct([1;3;5], [1;2;3], blkdiag(diag(11:13), magic(3)));
%! assert(v, [11;36;65], tol);
%! assert(i, [28;34;28], tol);
