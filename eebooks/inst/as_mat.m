%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: MAT = as_mat (DS, RD, RS, F, GND)
%%
%% Compute Green's function matrix in vacuum.
%%
%% See also: as_matf.

function mat = as_mat(ds, rd, rs, f=0, gnd=-1)

  if (gnd)
    rm = as_mirror(rs);
    mat = as_mat(ds, rd, rs, f, 0) + as_mat(ds, rd, rm, f, 0) * gnd;
    return
  end

  switch (ds)
  case {'vi','aq'}
    mat = zeros(rows(rd), rows(rs));
    return
  case 'vq'
    [xd,yd,zd] = as_node(rd);
    [xs,ys,zs] = as_node(rs);
    r = max(0.5, hypot(xd-xs.', yd-ys.', zd-zs.'));
    mat = (1/4/pi./r) .* exp(-2j*pi*f.*r);
    return
  case 'ai'
    [rn,rp] = as_branch(rd); rd = (rp+rn)/2; ld = rp-rn;
    [rn,rp] = as_branch(rs); rs = (rp+rn)/2; ls = rp-rn;
    [xd,yd,zd] = as_node(ld);
    [xs,ys,zs] = as_node(ls);
    mat = as_mat('vq', rd, rs, f, 0) .* (xd*xs.' + yd*ys.' + zd*zs.');
    return
  end

  switch (ds(1))
  case 'e'
    [rn,rp] = as_branch(rd);
    mat = as_mat(['v' ds(2)], rn, rs, f, gnd)...
        - as_mat(['v' ds(2)], rp, rs, f, gnd)...
        - as_mat(['a' ds(2)], rd, rs, f, gnd) * (2j*pi*f);
    return
  case 'b'
    [r1,r2,r3,r4] = as_loop(rd);
    mat = as_mat(['a' ds(2)], r1, rs, f, gnd)...
        + as_mat(['a' ds(2)], r2, rs, f, gnd)...
        + as_mat(['a' ds(2)], r3, rs, f, gnd)...
        + as_mat(['a' ds(2)], r4, rs, f, gnd);
    return
  end

  switch (ds(2))
  case 'p'
    [rn,rp] = as_branch(rs);
    mat = as_mat([ds(1) 'q'], rd, rp, f, gnd)...
        - as_mat([ds(1) 'q'], rd, rn, f, gnd)...
        + as_mat([ds(1) 'i'], rd, rs, f, gnd) * (2j*pi*f);
    return
  case 'm'
    [r1,r2,r3,r4] = as_loop(rs);
    mat = as_mat([ds(1) 'i'], rd, r1, f, gnd)...
        + as_mat([ds(1) 'i'], rd, r2, f, gnd)...
        + as_mat([ds(1) 'i'], rd, r3, f, gnd)...
        + as_mat([ds(1) 'i'], rd, r4, f, gnd);
    return
  end

end

function r = as_mirror(r)
  r(:,3:3:end) *= -1;
end

%!shared tol,N, RD,RS, f, RC,C, RL,L
%! tol = sqrt(eps);
%! N = 4;
%! RD.b = randn(N,12)*10; RD.a = RD.e = RD.b(:,1:6); RD.v = RD.b(:,1:3);
%! RS.m = randn(N,12)*10; RS.i = RS.p = RS.m(:,1:6); RS.q = RS.m(:,1:3);
%! RC = randn(N, 6)*10; C = +rande(N,1);
%! RL = randn(N,12)*10; L = -rande(N,1);
%! f = rande;

%!xtest
%! for gnd = -1:1 for [rd,d] = RD for [rs,s] = RS
%!   assert(as_matf([d s],rd,rs,f,gnd), as_mat([d s],rd,rs,f,gnd));
%! end end end

%!xtest
%! for gnd = -1:1 for [rd,d] = RD for [rs,s] = RS
%!   M = as_matf([d s],rd,rs,f,gnd,RC,C,RL,L);
%!   for i = 1:N for j = 1:N
%!     assert(M(i,j), as_matf([d s],rd(i,:),rs(j,:),f,gnd,RC,C,RL,L), tol);
%!   end end
%! end end end

%!xtest
%! %% reciprocal
%! for gnd = -1:1
%!   assert(issymmetric(as_matf('vq',RD.v,RD.v,f,gnd,RC,C,RL,L), tol));
%!   assert(issymmetric(as_matf('ep',RD.e,RD.e,f,gnd,RC,C,RL,L), tol));
%!   assert(issymmetric(as_matf('ai',RD.a,RD.a,f,gnd,RC,C,RL,L), tol));
%!   assert(issymmetric(as_matf('bm',RD.b,RD.b,f,gnd,RC,C,RL,L), tol));
%! end

%!xtest
%! %% ground
%! t0 = [0 0 0]; [~,t1,n1] = as_setd(t0,1); [~,n2,t2] = as_setd(t0,2);
%! for [rs,s] = RS
%!   assert(as_matf(['v' s],t0,rs,f,-1,RC,C,RL,L), zeros(1,N), tol);
%!   assert(as_matf(['e' s],t1,rs,f,-1,RC,C,RL,L), zeros(1,N), tol);
%!   assert(as_matf(['e' s],n1,rs,f,+1,RC,C,RL,L), zeros(1,N), tol);
%!   assert(as_matf(['a' s],t1,rs,f,-1,RC,C,RL,L), zeros(1,N), tol);
%!   assert(as_matf(['a' s],n1,rs,f,+1,RC,C,RL,L), zeros(1,N), tol);
%!   assert(as_matf(['b' s],t2,rs,f,-1,RC,C,RL,L), zeros(1,N), tol);
%!   assert(as_matf(['b' s],n2,rs,f,+1,RC,C,RL,L), zeros(1,N), tol);
%! end

%!xtest
%! %% mirror
%! mir = repmat([1 1 -1], 1,4);
%! fun = @(r) r .* mir(1:columns(r));
%! QC = fun(RC); QL = fun(RL);
%! for [rd,d] = RD qd = fun(rd); for [rs,s] = RS qs = fun(rs);
%!   M1 = as_matf([d s],rd,rs,f,0,[RC;QC],[C;C],[RL;QL],[L;L]);
%!   M2 = as_matf([d s],rd,qs,f,0,[RC;QC],[C;C],[RL;QL],[L;L]);
%!   assert(as_matf([d s],rd,rs,f,+1,RC,C,RL,L), M1+M2, tol);
%!   assert(as_matf([d s],rd,rs,f,-1,RC,C,RL,L), M1-M2, tol);
%! end end

%!xtest
%! %% symmetry
%! rot = kron(eye(4), orth(randn(3)));
%! shf = repmat(randn(1,3), 1,4);
%! fun = @(r) r * rot(1:columns(r),1:columns(r)) + shf(1:columns(r));
%! QC = fun(RC); QL = fun(RL);
%! for [rd,d] = RD qd = fun(rd); for [rs,s] = RS qs = fun(rs);
%!   assert(as_matf([d s],rd,rs,f,0,RC,C,RL,L),
%!          as_matf([d s],qd,qs,f,0,QC,C,QL,L), tol);
%! end end
