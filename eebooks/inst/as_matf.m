%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: MAT = as_matf (DS, RD, RS, F, GND)
%% usage: MAT = as_matf (DS, RD, RS, F, GND, RC, C, RL, L)
%%
%% Compute Green's function matrix with media.
%%
%% DS specifies destination and source type.
%% DS(1) may be
%%   'v' for node voltage,
%%   'e' for branch voltage,
%%   'a' for branch linkage,
%%   'b' for loop linkage.
%% DS(2) may be
%%   'q' for node charge,
%%   'p' for branch charge,
%%   'i' for branch current,
%%   'm' for loop current.
%%
%% The relations between them are
%%
%%   P - Q -> V - E
%%     \        /
%%   M - I -> A - B
%%
%% A group of locations is specified by a real matrix which may be
%%    3-column [X,Y,Z] for nodes,
%%    6-column [R1,R2] for branches from nodes R1 to R2,
%%   12-column [R1,R2,R3,R4] for loops R1->R2->R3->R4->R1.
%%
%% RD is destination locations, ND-row real matrix.
%%
%% RS is source locations, NS-row real matrix.
%%
%% F is frequency; default is 0.
%%
%% GND is type of ground plane Z=0,
%%    0 for none,
%%    1 for perfect magnetic conductor,
%%   -1 for perfect electric conductor (default).
%%
%% RC is branch media locations, NC-by-6 real matrix.
%%
%% C is branch electric susceptance, NC-element vector,
%% or branch electric conductance divided by 2j*pi*F.
%%
%% RL is loop media locations, NL-by-12 real matrix.
%%
%% L is loop magnetic susceptance, NL-element vector.
%%
%% MAT is Green's function matrix, ND-by-NS complex matrix, assuming
%% that electric constant, magnetic constant and node diameter are 1.
%%
%% See also: as_matz.

function mat = as_matf(ds, rd, rs, f=0, gnd=-1, rc=zeros(0,6), c=[], rl=zeros(0,12), l=[])

  %%   [DP,DM]       [C,0;0,L]     [EP,EM;BP,BM]       [ES;BS]
  %% D <------ [P;M] <-------------------------> [E;B] <------ S
  %%
  %%                           -1               -1
  %%   [DP,DM]       ([C,0;0,L]  -[EP,EM;BP,BM])       [ES;BS]
  %% D <------ [P;M] <-------------------------- [E;B] <------ S

  assert(rows(rc), numel(c));
  assert(rows(rl), numel(l));

  DS = as_mat(ds, rd,rs, f,gnd);
  DM = [as_mat([ds(1) 'p'], rd,rc, f,gnd), as_mat([ds(1) 'm'], rd,rl, f,gnd)];
  MS = [as_mat(['e' ds(2)], rc,rs, f,gnd); as_mat(['b' ds(2)], rl,rs, f,gnd)];
  MM = [as_mat('ep', rc,rc, f,gnd), as_mat('em', rc,rl, f,gnd);
        as_mat('bp', rl,rc, f,gnd), as_mat('bm', rl,rl, f,gnd)];
  MI = diag(1./[c(:); l(:)]);

  mat = DS + DM / (MI - MM) * MS;

end

%!demo
%! close all;
%! dr = -3:3;
%!
%! rq = [0 0 0];
%! [~,~,ri] = as_setd(rq, 1);
%! C = as_matf('vq', rq,rq, 0,0)^-1;
%! L = as_matf('ai', ri,ri, 0,0);
%! figure; barh([C 4*pi*0.5; nan nan]); yticks([]);
%! xlabel('C'); title('node capacitance');
%! figure; barh([L 1/(4*pi*0.5); nan nan]); yticks([]);
%! xlabel('L'); title('branch inductance');
%!
%! rv = as_setg(dr,dr,dr, 0);
%! [xe,ye,ze] = as_setd(rv, 1); [xb,yb,zb] = as_setd(rv, 2);
%! V = as_matf('vq', rv,rq, 0,0);
%! E = reshape(as_matf('eq', [xe;ye;ze],rq, 0,0), [],3);
%! A = reshape(as_matf('ai', [xe;ye;ze],ri, 0,0), [],3);
%! B = reshape(as_matf('bi', [xb;yb;zb],ri, 0,0), [],3);
%! figure; as_plotd(rv, 4*pi*V); hold on; as_plotd(rv, 4*pi*E); hold off;
%! legend('V','E'); title('node voltage distribution');
%! figure; as_plotd(rv, 4*pi*A); hold on; as_plotd(rv, 4*pi*B); hold off;
%! legend('A','B'); title('branch linkage distribution');

%!demo
%! close all;
%! f = 0.1; dr = 2/f:3/f; dt = dp = 5; deg = 0:15:359;
%!
%! rq = [0 0 0];
%! [~,~,rp] = as_setd(rq, 1); [~,~,rm] = as_setd(rq, 2);
%! R = -real(as_matf('ep', rp,rp, f,0) / (2j*pi*f));
%! figure; barh([R (2*pi*f)^2/(6*pi); nan nan]); yticks([]);
%! xlabel('R'); title('branch impedance');
%! R = +real(as_matf('bm', rm,rm, f,0) * (2j*pi*f));
%! figure; barh([R (2*pi*f)^4/(6*pi); nan nan]); yticks([]);
%! xlabel('R'); title('loop impedance');
%!
%! [r,t] = ndgrid(dr, deg2rad([0,45,45,90,90,90]));
%! [r,p] = ndgrid(dr, deg2rad([0, 0,90, 0,45,90]));
%! [x,y,z] = sph2cart(p, pi/2-t, r); rv = [x(:) y(:) z(:)];
%! [xe,ye,ze] = as_setd(rv, 1); [xb,yb,zb] = as_setd(rv, 2);
%! E = reshape(as_matf('ep', [xe;ye;ze],rp, f,0), [],3) / (2j*pi*f);
%! B = reshape(as_matf('bp', [xb;yb;zb],rp, f,0), [],3) / (2j*pi*f);
%! figure;  as_plotd(rv, dr(1)/f*4*pi/(2*pi*f)*E, -90);
%! hold on; as_plotd(rv, dr(1)/f*4*pi/(2*pi*f)*B, -90); hold off;
%! legend('E','B'); title('branch voltage/linkage distribution');
%! E = reshape(as_matf('em', [xe;ye;ze],rm, f,0), [],3);
%! B = reshape(as_matf('bm', [xb;yb;zb],rm, f,0), [],3);
%! figure;  as_plotd(rv, dr(1)/f*4*pi/(2*pi*f)^2*E, 0);
%! hold on; as_plotd(rv, dr(1)/f*4*pi/(2*pi*f)^2*B, 0); hold off;
%! legend('E','B'); title('loop voltage/linkage distribution');
%!
%! [r,t] = ndgrid(dr(1), deg2rad([0,45,45,90,90,90]));
%! [r,p] = ndgrid(dr(1), deg2rad([0, 0,90, 0,45,90]));
%! [x,y,z] = sph2cart(p, pi/2-t, r); rv = [x(:) y(:) z(:)];
%! [xe,ye,ze] = as_setd(rv, 1); [xb,yb,zb] = as_setd(rv, 2);
%! E = reshape(as_matf('ep', [xe;ye;ze],rp, f,0), [],3) / (2j*pi*f);
%! B = reshape(as_matf('bp', [xb;yb;zb],rp, f,0), [],3) / (2j*pi*f);
%! figure;  as_plotd(rv, dr(1)/f*4*pi/(2*pi*f)*E, deg);
%! hold on; as_plotd(rv, dr(1)/f*4*pi/(2*pi*f)*B, deg); hold off;
%! legend('E','B'); title('branch voltage/linkage polarization map');
%! E = reshape(as_matf('em', [xe;ye;ze],rm, f,0), [],3);
%! B = reshape(as_matf('bm', [xb;yb;zb],rm, f,0), [],3);
%! figure;  as_plotd(rv, dr(1)/f*4*pi/(2*pi*f)^2*E, deg);
%! hold on; as_plotd(rv, dr(1)/f*4*pi/(2*pi*f)^2*B, deg); hold off;
%! legend('E','B'); title('loop voltage/linkage polarization map');
%!
%! [t,p,r] = ndgrid(deg2rad(0:dt:180), deg2rad(dp:dp:360), dr(end));
%! [x,y,z] = sph2cart(p, pi/2-t, r); rv = [x(:) y(:) z(:)];
%! [xe,ye,ze] = as_setd(rv, 1); [xb,yb,zb] = as_setd(rv, 2);
%! E = reshape(as_matf('ep', [xe;ye;ze],rp, f,0), [],3) / (2j*pi*f);
%! figure; as_plotd(rv, 4*pi/(2*pi*f)*sqrt(dot(E,E,2)));
%! legend('|E|'); title('branch voltage radiation pattern');
%! E = reshape(as_matf('em', [xe;ye;ze],rm, f,0), [],3);
%! figure; as_plotd(rv, 4*pi/(2*pi*f)^2*sqrt(dot(E,E,2)));
%! legend('|E|'); title('loop voltage radiation pattern');

%!demo
%! close all;
%! cl = 6; cw = 8; ch = 5; cm = 1e+3;
%!
%! [rx,ry,rz] = as_setg(1:cl, 1:cw, 0:ch, 1);
%! rc = [rx;ry;rz]; c = (cm-1)(ones(rows(rc),1));
%! r1 = as_setg(1:cl, 1:cw,  0, 0);
%! r2 = as_setg(1:cl, 1:cw, ch, 0);
%! figure; as_plotr(rc); hold on; as_plotr([r1;r2]); hold off;
%! legend('C','P'); title('plate capacitor structure');
%!
%! M = as_matz(repelem(1:2, [rows(r1) rows(r2)]));
%! C = as_maty(M.' / as_matf('vq', [r1;r2],[r1;r2], 0,0, rc,c) * M);
%! figure; barh([C(1,2) cm*cl*cw/ch; nan nan]); yticks([]);
%! xlabel('C'); title('plate capacitor capacitance');

%!demo
%! close all;
%! ll = 6; lw = 8; lh = 5; lm = 1e-3;
%!
%! [rx,ry,rz] = as_setg(0:ll, 0:lw, 1:lh, 2);
%! rl = [rx;ry;rz]; l = (1-1/lm)(ones(rows(rl),1));
%! [r1,~] = as_setg(   0:ll,       0, 1:lh, 1);
%! [~,r2] = as_setg(     ll,    0:lw, 1:lh, 1);
%! [r3,~] = as_setg(ll:-1:0,      lw, 1:lh, 1);
%! [~,r4] = as_setg(      0, lw:-1:0, 1:lh, 1);
%! r1 = [r1;r2;r3;r4];
%! figure; as_plotr(rl); hold on; as_plotr(r1); hold off;
%! legend('L','P'); title('spiral inductor structure');
%!
%! M = as_matz(ones(rows(r1),1));
%! L = M.' * as_matf('ai', r1,r1, 0,0, :,:,rl,l) * M;
%! figure; barh([L lm*ll*lw*lh; nan nan]); yticks([]);
%! xlabel('L'); title('spiral inductor inductance');

%!demo
%! close all;
%! lr = 3; lh = 100; ln = 50;
%!
%! p = linspace(0, 2*pi*ln, 2*pi*ln*lr); z = linspace(0, lh, numel(p));
%! [x,y] = pol2cart(p, lr); r1 = [x(:) y(:) z(:)]; r2 = r1(1:end/2,:);
%! r1 = [r1(1:end-1,:),r1(2:end,:)]; r2 = [r2(2:end,:),r2(1:end-1,:)];
%! figure; as_plotr(r1); hold on; as_plotr(r2); hold off;
%! legend('P1','P2'); title('transformer structure');
%!
%! M = as_matz(repelem(1:2, [rows(r1) rows(r2)]));
%! L = M.' * as_matf('ai', [r1;r2],[r1;r2], 0,0) * M;
%! figure; barh([vech(L) pi*lr^2*ln^2/lh./[1;-2;2]]); yticks([]);
%! xlabel('L'); title('transformer self/mutual inductance');

%!demo
%! close all;
%! ll = 6; lw = 8; lm = 1e-3; cm = 4e+6; gm = 2.5e+3; fs = logspace(-5,-1,21);
%!
%! [~,~,rl] = as_setg(0:ll, 0:lw, 0, 2); l = (1-1/lm)(ones(rows(rl),1));
%! [~,rc] = as_setg( 0, 0:lw, 0, 1); c = (cm-1)(ones(rows(rc),1));
%! [~,rg] = as_setg(ll, 0:lw, 0, 1); g = gm(ones(rows(rg),1));
%! rs = [as_setg(1:ll,0,0, 1); as_setg(0:ll,lw,0, 1)]; s = inf(rows(rs),1);
%! r1 = as_setg(0:1, 0, 0, 1);
%! figure; as_plotr(rl); hold on; as_plotr(rc); as_plotr(rg);
%! as_plotr(rs); as_plotr(r1); hold off;
%! legend('L','C','G','Inf','P'); title('GCL tank structure');
%!
%! z = []; for f = fs
%!   z(end+1) = -as_matf('ep', r1,r1, f,0,
%!     [rc;rg;rs],[c;g/(2j*pi*f);s], rl,l) / (2j*pi*f);
%! end
%! g0 = gm/lw; c0 = cm/lw; l0 = lm*ll*lw;
%! z0 = 1/g0 + 2j*pi*fs*l0 + 1./(2j*pi*fs*c0);
%! figure;
%! subplot(212); semilogx(fs,arg(z),'x', fs,arg(z0));
%! xlabel('f'); ylabel('\phi_Z');
%! subplot(211); loglog(fs,abs(z),'x', fs,abs(z0));
%! xlabel('f'); ylabel('|Z|');
%! title('GCL tank impedance Bode-plot');

%!demo
%! close all;
%! sl = 250; fs = linspace(1e-5,1,21) / sl;
%!
%! rq = as_setg(0:sl, 0, 1, 0);
%! rs = as_setg(0:sl, 0, 1, 1); s = inf(rows(rs),1);
%! [~,~,r1] = as_setg( 0, 0, 0:1, 1);
%! [~,~,r2] = as_setg(sl, 0, 0:1, 1);
%! figure; as_plotr(rs); hold on; as_plotr([r1;r2]); hold off;
%! legend('Inf','P'); title('microstrip structure');
%!
%! M = as_matz(ones(rows(rs),1));
%! L = M.' * as_matf('ai', rs,rs) * M;
%! M = as_matz(ones(rows(rq),1));
%! C = M.' / as_matf('vq', rq,rq) * M;
%! Z = sqrt(L/C);
%! figure; barh([L/sl sl/C log(4)/(2*pi); nan(1,3)]); yticks([]);
%! xlabel('Z_c'); title('microstrip impedance');
%!
%! zo = zm = []; for f = fs
%!   z = -as_matf('ep', [r1;r2],[r1;r2], f,:, rs,s) / (2j*pi*f);
%!   zo(end+1) = z(1,1);
%!   y = as_maty(z^-1); y(2,2) += 1/Z; z = as_maty(y)^-1;
%!   zm(end+1) = z(1,1);
%! end
%! sm = (zm-Z)./(zm+Z); sm0 = zeros(size(fs));
%! so = (zo-Z)./(zo+Z); so0 = exp(-4j*pi*fs*sl);
%! figure; plot3(fs,sm,'x', fs,sm0); ylim([-1,1]); zlim([-1,1]);
%! ylabel('Re \Gamma'); zlabel('Im \Gamma'); xlabel('f');
%! title('microstrip matched impedance Smith-chart');
%! figure; plot3(fs,so,'x', fs,so0); ylim([-1,1]); zlim([-1,1]);
%! ylabel('Re \Gamma'); zlabel('Im \Gamma'); xlabel('f');
%! title('microstrip open impedance Smith-chart');
