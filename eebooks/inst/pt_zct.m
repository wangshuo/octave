%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: [A, B] = pt_zct (V, I, ZC)
%%
%% Compute the travelling wave transform.
%% ZC is wave mode; default is of 50-Ohm uncoupled transmission line.
%%
%% See also: pt_izct, pt_zc.

function [A, B] = pt_zct(V, I, ZC=pt_zc(50*eye(rows(V))))
  assert(size(V), size(I));
  assert(issquare(ZC));
  assert(rows([V; I]), length(ZC));
  X = [V; I];
  X = ZC \ X;
  A = X(1:end/2,:);
  B = X(end/2+1:end,:);
end

%!function check(tol, v, i, varargin)
%! [a, b] = pt_zct(v, i, varargin{:});
%! [v_, i_] = pt_izct(a, b, varargin{:});
%! assert(v_, v, tol);
%! assert(i_, i, tol);
%!endfunction

%!test
%! tol = eps^0.5;
%! check(tol, 11, 21);
%! check(tol, 11, 21, [2 3;5 7]);
%! check(tol, (11:13).', (21:23).');
%! check(tol, (11:13).', (21:23).', blkdiag(diag(11:13), magic(3)));
