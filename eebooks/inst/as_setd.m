%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: R = as_setd (R, D)
%% usage: [RX, RY, RZ] = as_setd (...)
%%
%% Generate detector locations.
%%
%% R is locations, ND-by-3 real matrix.
%%
%% D specifies detector type,
%%   0 for node detector,
%%   1 for branch detector,
%%   2 for loop detector.
%%
%% R is node detector locations, ND-by-3 real matrix.
%% RX, RY, RZ are branch/loop detector locations, ND-row real matrix.
%%
%% See also: as_matf, as_plotd.

function [rx,ry,rz] = as_setd(r, d)

  switch (d)
  case 0
    rx = r;
  case 1
    r = repmat(r, 1,2);
    rx = r + [-0.5 0 0, 0.5 0 0];
    ry = r + [0 -0.5 0, 0 0.5 0];
    rz = r + [0 0 -0.5, 0 0 0.5];
  case 2
    r = repmat(r, 1,4);
    rx = r + [0 -0.5 -0.5, 0 0.5 -0.5, 0 0.5 0.5, 0 -0.5 0.5];
    ry = r + [-0.5 0 -0.5, -0.5 0 0.5, 0.5 0 0.5, 0.5 0 -0.5];
    rz = r + [-0.5 -0.5 0, 0.5 -0.5 0, 0.5 0.5 0, -0.5 0.5 0];
  end

end
