%% Copyright (C) 2012  Free E&E
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

%% usage: CIR = as_maty (MAT)
%% usage: MAT = as_maty (CIR)
%%
%% Compute circuit admittor from admittance matrix, or vice versa.
%%
%% MAT is admittance matrix.
%% CIR(I,J) is the admittor between port I and J, and
%% CIR(I,I) is the admittor between port I and ground.
%% Both are NP-by-NP symmetric complex matrix.
%%
%% See also: as_matf.

function cir = as_maty(mat)

  assert(issymmetric(mat, sqrt(eps)));

  cir = diag(sum(mat)) + diag(diag(mat)) - mat;

end

%!test
%! tol = sqrt(eps);
%! N = 0:4;
%! Y = randn(N(end)); Y = Y + Y.';
%! for n = N
%!   y = Y(1:n, 1:n);
%!   assert(as_maty(as_maty(y)), y, tol);
%! end
